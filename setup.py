from setuptools import find_packages, setup

requirements = [
    "tensorflow",
    "numpy",
    "dask",
    "distributed",
    "imageio",
    "yapsy",
    "pyyaml",
    "scikit-image",
    "pyzmq",
    "voluptuous",
    "pydicom",
    "tqdm",
    "imbalanced-learn",
    "keras-tuner",
    "dash",
    "dash-bootstrap-components",
    "dash-bootstrap-templates",
    "plotly",
    "ydata-profiling"
]

setup_requirements = ["pytest-runner", "flake8"]

test_requirements = ["coverage", "pytest", "pytest-cov", "pytest-mock"]

setup(
    author="AIX-COVNET",
    maintainer="Eduardo Gonzalez Solares, Ian Selby",
    maintainer_email="eg266@cam.ac.uk, ias49@cam.ac.uk",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python :: 3.7",
    ],
    description="AutoQC",
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description="",
    include_package_data=True,
    keywords="covid19",
    name="auto-qc",
    packages=find_packages(),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    entry_points={
        "console_scripts": [
            "autoqc=auto_qc.cli:main",
            "autocrop=auto_qc.operations.boundary:main",
        ]
    },
    url="https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/autoqc",
    version="0.7.0",
    zip_safe=True,
    python_requires=">=3.7",
)
