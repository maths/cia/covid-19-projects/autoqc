---
myst:
  html_meta:
    "description lang=en": |
      User guide for AutoQC.
---

# Optical Character Recognition (OCR)

:::{button-ref} buttons
:color: primary
:outline:
AutoQC Stage 4
:::

Our optical character recognition (OCR) pipeline is designed to extract the
text from chest radiographs. This has been tested for frontal projection, but
other relevant information is extracted, including patient positioning (e.g. ERECT, SUPINE, PRONE),
whether it is a portable/mobile image, whether in an ITU/ICU, scanner settings, etc.

The OCR pipeline utilises the text removal code and the 
[Tesseract OCR](https://tesseract-ocr.github.io/tessdoc/) engine.

## Example Usage

The OCR package must be run in two stages:

1. Obtain the masks:

::::{tab-set}
:::{tab-item} Console
:sync: console
```console
autoqc inpaint <input_image> --method extract_text
```
:::
:::{tab-item} Python
:sync: python
```python
import imageio.v2 as iio
from auto_qc.operations.inpaint import inpaint_cxr

image = iio.imread("path/to/image")
mask = inpaint_cxr(image, method="extract_text")
```
:::
::::

2. Run the OCR pipeline:

::::{tab-set}
:::{tab-item} Console
:sync: console
```console
autoqc read_mask <input_mask>
```
The output is a json file containing the OCR results for the image.
:::
:::{tab-item} Python
:sync: python
```python
from auto_qc.operations.ocr import run_ocr

ocr_out_dict = run_ocr("path/to/mask.png")
```

The output will be a dictionary containing the OCR results for the image.
:::
::::

The OCR output can be cleaned and categorised using the following python code:

```python
import pandas as pd
from importlib.resources import open_text
from auto_qc.operations.ocr import clean_data, find_terms, create_dictionaries

ocr_out_dict = "output from above or processed json file"
cleaned_ocr = clean_data(ocr_out_dict)

with open_text('auto_qc', 'dict_table.csv') as f:
    dict_df = pd.read_csv(f, dtype=str)
combined_dict, eng_dict, spa_dict, ita_dict, port_dict = create_dictionaries(dict_df)
ocr_out = find_terms(cleaned_ocr, combined_dict)
```

The `evaluate.py` module was utilsed to evaluate the OCR pipeline for classifying frontal 
projection, with the config file as for the other tools:

```yaml
# eval_ocr_projection.yaml

task: ocr

ocr_only_use_if_pred_made: true

datasets: all

dirs:
  dev_labels: path_to_dev_labels_directory
  test_labels: path_to_test_labels_csv
  inference: path_to_directory_to_output_jsons_for_each_dataset
  output: path_to_directory_to_store_output

exclude_unsuitable: true
nbootstrap: 1000
nbatch: 8

use_gpu: false
multi_cpu: true
nworkers: 60
```
