---
myst:
  html_meta:
    "description lang=en": |
      User guide for AutoQC Data Explorer.
---

# Profile Reports

Produced using [YData Profiling](https://docs.profiling.ydata.ai/), these reports provide a summary of the data
including the number of missing values, the number of unique values, the most frequent values and the data types
of each column.

## Settings

As this process can be computationally expensive, the settings can be changed to reduce the number of columns
analysed and the number of rows sampled.

| Setting                   | Description                                                                                                                                                         |
|---------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `Minimal Report`          | Actives the 'minimal' option in YData, where the most expensive computations are turned off by default. This is the recommended starting point for larger datasets. |
| `Explorative`             | Activates the 'explorative' option in YData, the explorative mode, a lightweight data profiling option.                                                             |
| `Focused Report`          | Use only subset of columns deemed to be some of the most important in this context.                                                                                 |
| `Continuous Interactions` | Whether to calculate the interactions between continuous variables (off by default).                                                                                |
| `Sample Report`           | Whether to sample the data to be reported upon.                                                                                                                     |
| `Sample Size`             | The number of rows to sample from the data.                                                                                                                         |
| `Seed`                    | The seed used to sample the data for reproducibility.                                                                                                               |

# Comparison Reports

Allow for the data to be split by a categorical column, e.g. dataset, diagnosis or outcome, and 
the subsets of data to be compared. This allows for the user to explore potential imbalances in the data
or trends which may result in bias or shortcuts being learnt by subsequently developed models using the
data.

The settings can be changed using the dropdown menu as described in the Profile Reports section above.
