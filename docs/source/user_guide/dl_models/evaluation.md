---
myst:
  html_meta:
    "description lang=en": |
        AutoQC Deep Learning Models.
---

# Model Evaluation

The `evaluate.py` script is used to evaluate a binary classification model on a dataset. It will:

- Produce plots of the training metrics by epoch.
- Calibrate the model and find the threshold to maximise the F1 score. It saves the calibrated 
classifier, calibration curves, and the F1 score at each threshold.
- Produce plots of the ROC and precision-recall curves for the validation set.
- Perform a basic analysis of the test labels by dataset, scanner manufacturer and Covid-19 status, saving plots as png 
files (*the user can turn this off in the configuration settings if not a Covid-19 dataset*).
- Perform inference on the test sets.
- Produce confusion matrices for each test set.
- Provide confidence intervals for all metrics.
- Plot examples of model failures.
- Plot GradCam heatmaps.

The script will perform similar actions for RotNet models.

## Running the evaluate.py script

To run the evaluation of a model, you can use the following:

::::{tab-set}
:::{tab-item} Console
```console
autoqc evaluate --config <path to config file>
```
Help is available using ```autoqc evaluate --help```.
:::

:::{tab-item} Python

To run with a configuration file (see below):

```python
from argparse import Namespace
from auto_qc.scripts.evaluate import evaluate

args = Namespace(config_path="/path/to/config/train.yaml")
evaluate(args)
```

To run with a configuration dictionary:

```python
from auto_qc.scripts.evaluate import evaluate_using_dict

config_dict = {
    ...: ...
}
evaluate_using_dict(config_dict)
```

The configuration dictionary should be of the same format as the configuration file (see below).
Help is available using ```help(evaluate)``` and ```help(evaluate_using_dict)```.
:::
::::

```{hint}
A Jupyter notebook is provided in the GitLab repository to demonstrate how to use the individual components of 
`evaluate.py` script to allow the user to customise behaviour and outputs.
```

## Configuration files

An example configuration file:

```yaml
# config.yaml

task: projection          # The task to evaluate. Other options: inverted, lateral, rotation, projection, pacemakers

datasets: all            # The datasets to evaluate on. Options: all, all_external, nccid, bimcv, cuh, brixia, ricord

dirs:
  checkpoint: /path/to/model_projection.h5      # The model to evaluate
  logfile: /path/to/history_projection.csv      # Contains the training metrics
  evalfile: /path/to/model_projection_eval.json    # The evaluation results from training
  dev_labels: /path/to/dev/labels/directory     # This should contain a csv file with the labels for the dev set for 
                                                # the relevant stage of the pipeline, e.g. dev_labels_stage2.csv
  test_labels: /path/to/external/labels/external_testset_labels.csv   # This should contain a csv file with the labels 
                                                                      # for the test set. Unlike the dev set, this
                                                                      # should contain all the labels for all stages
  inference: /path/to/inference/output/directory  # This will contain the inference results produced by the script
  output: /path/to/evaluate/output/directory    # This will contain the plots and csv files produced by the script

run_training_evaluator: true      # Produce the training metrics plots
calibrate: true                   # Calibrate the model and find the threshold to maximise the F1 score
run_demogs: true                  # Run the demographics analysis
run_holdout_evaluator: true       # Run the evaluator on the holdout set

exclude_unsuitable: true          # Exclude unsuitable scans from the evaluation, e.g. based on aspect ratio 
nbootstrap: 1000                  # Number of bootstrap samples to use for confidence intervals
nbatch: 8                         # Number of samples to use per batch for inference

use_gpu: false                    # Use GPU for inference
multi_cpu: true                   # Use multiple CPUs for inference
nworkers: 40                      # Number of CPU workers to use
```
