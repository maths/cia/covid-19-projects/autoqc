---
myst:
  html_meta:
    "description lang=en": |
        AutoQC Deep Learning Models.
---

# Model Architecture

## Generic Classification Model

This model is a convolutional neural network designed in [Keras](https://keras.io/) for either binary or multi-class 
classification tasks. It initiates with an input layer configured with a specified input_shape (256x256 for AutoQC).

To augment the data and potentially enhance the model's performance, the network integrates 
random rotations, width alterations, height changes, and zoom adjustments, contingent on the 
augmentation parameter being set to true (not used in AutoQC). Following this, the image undergoes 
resizing, either through cropping or direct resizing to dimensions specified by imgsize.

Within the core of the network, a loop iterates *nlayers* times, progressively increasing the number 
of filters in the convolutional layers by adding 16 filters in each iteration. Inside this, 
another loop iterates over *nblocks*, creating a convolutional block each time. Inside each 
block, a convolutional layer with ReLU activation is applied, followed by either max pooling 
and stride of 2 or a second convolution layer with stride 1, depending on the *downsample* 
variable. This is paired with skip connections where the output from the convolutional layer 
is added to the input, promoting better gradient flow during training. After the addition, a 
ReLU activation function is applied to introduce non-linearity.

Following the convolutional layers, the network applies global average pooling to reduce 
the spatial dimensions of the feature maps, condensing them into a feature vector. Depending 
on the *dropout* parameter, a dropout layer with a rate of 0.25 might be included to mitigate 
overfitting.

Finally, a fully connected dense layer with *nunits* nodes and ReLU activation is introduced 
before the output layer. Depending on the number of classes (*nclasses*), the output layer 
utilizes either a sigmoid (for binary classification) or softmax (for multi-class classification) 
activation function to make predictions.

The model encapsulates these configurations and layers and returns a compiled model ready for 
training.

```{figure} ../../_static/convnet.png
  :figwidth: 50%
  :alt: Generic model architecture (ConvNet)
  
  Generic model architecture (ConvNet).
```

All models use early stopping using the value of the validation loss with patience of 5.

The following are treated as hyperparameters in model tuning:

- number of layers (*nlayers*)
- internal convolutional blocks (*nblocks*)
- number of filters (*nfilters*) used in the convolutions
- data augmentation
- image cropping
- inclusion of a dropout layer (*dropout*) to prevent overfitting
- number of units in the last layer (*nunits*)
- initial learning rate

```{note}
Whether to include early stopping, data augmentation and image cropping as a hyperparameter can be specified in the
config file.
```

```{warning}
The BaseNet model allows for multi-class classification, but the current implementation of the AutoQC package
only supports binary classification (ConvNet). However, the user may utilise the BaseNet model for multi-class
by creating a custom file in the `models` folder and specifying the model name in the config file.

A future release will include multi-class classification more natively.
```

## Rotation Model

This Keras [Sequential](https://keras.io/guides/sequential_model/) model, named "RotNet", is structured to classify 
the rotation angle of images, with output classes representing 360 different angles (degrees). The architecture starts 
with a 2D convolution layer with a specified number of filters (*nfilters*) and a 3x3 kernel size, followed by a 
MaxPooling layer to downsample the input representation, keeping the most prominent features.

Depending on the value of *nlayers*, an additional convolution block may be added which contains a 2D convolution 
layer with increased filters (*nfilters* + 16) and a subsequent MaxPooling layer.

A conditional dropout layer can be incorporated to prevent overfitting, where a proportion of 0.25 of the 
nodes are ignored during training, encouraging the network to learn more robust features. This is followed by a 
Flatten layer to transform the 2D feature maps into a 1D feature vector, which then feeds into a Dense layer with a 
specified number of units (*nunits*) and ReLU activation function, introducing non-linearity into the model.

If the *dropout* condition is true, another dropout layer with a ratio of 0.25 is added before the final dense 
layer with 360 units and a softmax activation function. This final layer classifies the input into one of the 360 
different classes representing each degree of rotation.

The model employs the Adam optimizer with a specified learning rate and a custom *angle error* metric for performance 
evaluation during training.

```{figure} ../../_static/rotnet.png
  :figwidth: 50%
  :alt: RotNet architecture
  
  RotNet architecture.
```

