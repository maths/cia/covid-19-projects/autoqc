Model Prediction
================

Given a model and a list of images in a file we can perform inference using the following:

.. tab-set::

    .. tab-item:: Console

        .. code-block:: console

            autoqc predict <path to model checkpoint> <path to image list> --batch <batch size>

        where the batch argument sets the number of images in one batch.

    .. tab-item:: Python

        .. code-block:: python

            import tensorflow as tf
            from auto_qc.utils import imread
            from auto_qc.models.layers import CropAndResize
            from auto_qc.models.metrics import f1

            model = tf.keras.models.load_model(model_name, custom_objects={'CropAndResize': CropAndResize, 'f1': f1})
            img = imread(img_name)
            predictions = model.predict(img)

.. tip::

    Inference is performed automatically in the evaluate submodule.
