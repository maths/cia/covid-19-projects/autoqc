---
myst:
  html_meta:
    "description lang=en": |
      AutoQC development.
---

# Workflow

The AutoQC pipeline currently consists of four stages:

:::{figure} ../../_static/pipeline_pipes_v2.png
:alt: AutoQC pipeline
:auto-fig-width: true
An illustration of the AutoQC pipeline with four example radiographs at each stage of the pipeline.
:::

::::{grid} 1 1 1 1
:::{card} Stage 1
* Application of look-up tables or auto-windowing.
* Detection and correction of inverted grayscale.
:::
:::{card} Stage 2
* Removal of text annotations with inpainting or bounding boxes.
* Cropping padding / borders.
* Lateral projection / view detected or removed.
* Detection or removal of images unlikely to contain the two thirds of the lungs.
:::
:::{card} Stage 3
* Images rotated by a 90 degree angle (90, 180 or 270 deg) are corrected.
:::
:::{card} Stage 4
* Image quality is scored with outliers detected or removed.
* Reading of text / annotations on the images.
* Frontal projection / view labelled.
* Pacemakers labelled.
:::
::::

:::{figure} ../../_static/autoqc_workflow.png
:alt: AutoQC pipeline
:auto-fig-width: true
The AutoQC workflow.
:::

The package also includes a number of tools for visualising and analysing the results of the pipeline
and the image metadata.
