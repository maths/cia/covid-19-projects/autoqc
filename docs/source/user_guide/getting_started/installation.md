---
myst:
  html_meta:
    "description lang=en": |
      Installation instructions for AutoQC.
---

# Installation

## Installing the Package with the Models

To install the package with the models, you will need [git lfs](https://git-lfs.com/) installed.

The package and dependencies may be installed using pip:
```console
pip install git+https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/autoqc
```

Alternatively, the git repository may be cloned and installed locally:
```console
git clone https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/autoqc
cd autoqc
pip install .
```

```{hint}
It is advised to install the package in a virtual environment (conda or venv) with python 3.7 or higher.
```

## Installing the Package without the Models

If you do not have git lfs installed, the models will not download when you use the commands above. However, if you have git lfs and do not want to download the models when installing / cloning, you can use the following:

```console
git clone --filter=blob:none https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/autoqc
cd autoqc
pip install .
```

Alternatively you can use the following command before installation:

```console
git config lfs.fetchexclude "trained_models/*.h5"
```


## Included Models

Two sets of models are included: 
 
1. The models used in our paper to allow for replication of the results:
   - Datasets: NCCID and BIMCV
   - Nomenclature: `model_<tool_name>_paper.h5`

Note that if using the models from the paper, you will need to change the model names in the config file accordingly.

```{admonition} Coming soon!

*2. The models retrained on all of our datasets (i.e. the development and test sets), which should demonstrate superior 
performance for use in practice.*
- *Datasets: NCCID, BIMCV, CUH, BrixIA, and MIDRC-RICORD-1c*
- *Nomenclature: `model_<tool_name>.h5`*
```

```{warning}
AutoQC remains under development. Use at your own risk and please report any issues on our 
[GitLab repository](https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/autoqc/-/issues).
```
