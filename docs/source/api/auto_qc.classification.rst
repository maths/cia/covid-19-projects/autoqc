auto\_qc.classification
=======================

.. toctree::
   :maxdepth: 4

   auto_qc.classification.live_qa

auto\_qc.classification.aspect\_ratio
-------------------------------------

.. automodule:: auto_qc.classification.aspect_ratio
   :members:
   :undoc-members:
   :show-inheritance:

auto\_qc.classification.boundary
--------------------------------

.. automodule:: auto_qc.classification.boundary
   :members:
   :undoc-members:
   :show-inheritance:

auto\_qc.classification.quality
-------------------------------

.. automodule:: auto_qc.classification.quality
   :members:
   :undoc-members:
   :show-inheritance:
