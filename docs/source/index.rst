.. raw:: html

   <style>
   .sidebar_secondary {
       remove: true;
   }
   </style>

.. image:: _static/autoqc_logo.png
   :class: only-dark
   :align: right
   :width: 200


.. image:: _static/autoqc_logo.png
   :class: only-light
   :align: right
   :width: 200

AutoQC
======
*An automated quality control pipeline for chest radiographs.*

.. button-ref:: badges-buttons
    :ref-type: ref
    :color: muted
    :shadow:

    **Version:** |version|

Designed for use *prior to training a machine learning model or performing inference*, AutoQC aims to:

- Expedite quality control on chest radiograph datasets, giving developers rapid insights into their data;
- Improve downstream model generalisation; and
- Reduce the influence of confounding factors and contribute towards ethical AI.

In pursuit of these goals, AutoQC will:

1. Perform initial preprocessing;
2. Detect images that may not be suitable for training or inference; and
3. Detect and label images for possible confounding factors, e.g. radiographic projection or the presence of a pacemaker.

.. admonition:: A pre-print of our paper is available on request:

    **Automated Quality Control of Chest Radiographs: Tools to Combat Shortcut Learning in COVID-19 Deep Learning Models.** (*submitted*)

    Selby IA., González Solares E., Breger A., et al. on behalf of the `AIX-COVNET Collaboration <https://covid19ai.maths.cam.ac.uk>`_.

    .. raw:: html

        <div style="text-align: center;">
            <button data-tf-slider="ifo65jAj" data-tf-position="right" data-tf-opacity="90" data-tf-iframe-props="title=AutoQC Pre-print Request" data-tf-auto-close="10000" data-tf-transitive-search-params data-tf-medium="snippet" style="all:unset;font-family:Helvetica,Arial,sans-serif;display:inline-block;max-width:100%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;background-color:#39ACCF;color:#fff;font-size:16px;border-radius:5px;padding:0 26px;font-weight:bold;height:40px;cursor:pointer;line-height:40px;text-align:center;margin:0;text-decoration:none;">Request pre-print!</button><script src="//embed.typeform.com/next/embed.js"></script>
        </div>

.. toctree::
   :hidden:

   user_guide/index


.. toctree::
   :hidden:

   api/index

