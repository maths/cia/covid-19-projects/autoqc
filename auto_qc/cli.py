"""
cli.py: Command Line Interface Module for auto_qc

This module serves as a command line interface (CLI) tool for an image processing and classification
pipeline. It provides a range of commands to carry out different steps in the pipeline,
including preprocessing of data, training of a machine learning model, serving the model,
making predictions with the model, performing image classification, and running the full pipeline.

Commands:
    - preprocess: Used for the data preparation stage. It requires a configuration file.
    - train: Used for training a machine learning model. It requires a configuration file.
    - serve: Used to serve the trained model. Optional arguments include port and number of servers.
    - predict: Used for making predictions with a trained model. Requires model name and a file
      containing a list of images.
    - evaluate: Used for evaluating a trained model.
    - classify: Used for classifying images. Requires type of classification and a file containing a
      list of images.
    - inpaint: Used for removing annotations from CXRs.
    - image_comparison: Used for creating comparison images to assess removal of annotations.
    - read_mask: Used for running OCR on image masks.
    - quality_scores: Used for calculating the quality of an image.
    - run: Runs the full pipeline on a single image. Requires the image file.
"""
import os
import sys
from argparse import ArgumentParser, FileType, Namespace, ArgumentTypeError
from typing import List

from .classification import calculate_quality
from .scripts import classify, pipeline, predict, preprocess, serve_model, train, evaluate, run_pipeline, \
    run_quality_tuner
from .operations import inpaint, create_comparison_images, read_masks, remove_padding
from .utils import valid_file, valid_directory, combine_figures_cl
from .data_explorer.app import run_data_explorer


def parse_args(input: List[str]) -> Namespace:
    """
    Parse the command line arguments.

    This function sets up the ArgumentParser, adds the necessary arguments for each command,
    and parses the input list of strings into a Namespace object.

    :param input: The list of strings to parse into command line arguments
    :return: A Namespace object containing the parsed command line arguments
    """

    def tuple_type(strings):
        # The input is a string of two comma-separated numbers.
        # This function converts the string to a tuple of two integers.
        str_nums = strings.split(',')
        try:
            nums = tuple(int(num) for num in str_nums)
        except ValueError:
            raise ArgumentTypeError('Tuple values must be integers')
        return nums

    def valid_metrics(metrics):
        valid_metrics_list = [
            'niqe',
            'brisque',
            'contrast_ratio',
            'contrast_ratio_cutout50',
            'contrast_ratio_cutout75',
            'entropy',
            'entropy_cutout50',
            'entropy_cutout75',
            'mean_intensity',
            'std_intensity',
            'var_intensity',
            'median_intensity',
            'ptp_intensity',
            'snr',
            'fft_contrast',
            'all'
        ]
        if metrics == 'all':
            return metrics
        for metric in metrics:
            if metric not in valid_metrics_list:
                raise ArgumentTypeError(f"Invalid metric '{metric}'. Valid metrics are {valid_metrics_list}")
        return metrics

    parser = ArgumentParser()
    subparsers = parser.add_subparsers()

    # Data preparation
    pproc = subparsers.add_parser("preprocess")
    pproc.add_argument("--config", required=True, type=FileType("r"))
    pproc.set_defaults(func=preprocess)

    # Training
    training = subparsers.add_parser("train")
    training.add_argument("--config", required=True, type=FileType("r"))
    training.set_defaults(func=train)

    # Serve model
    serve = subparsers.add_parser("serve")
    # serve.add_argument("model", type=str)
    serve.add_argument("--port", required=False, default=5560, type=int)
    serve.add_argument("--nservers", required=False, default=1, type=int)
    serve.set_defaults(func=serve_model)

    # Predict
    pred = subparsers.add_parser("predict")
    pred.add_argument("model", type=str)
    pred.add_argument("imglist", type=FileType("r"))
    pred.add_argument("--batch", required=False, default=10, type=int)
    pred.set_defaults(func=predict)

    # Model Evaluation
    eval = subparsers.add_parser("evaluate")
    eval.add_argument("--config", required=True, type=FileType("r"))
    eval.set_defaults(func=evaluate)

    # Classify
    clsfy = subparsers.add_parser("classify")
    clsfy.add_argument("classtype", type=str)
    clsfy.add_argument("imglist", type=FileType("r"))
    clsfy.add_argument("--batch", required=False, default=10, type=int)
    clsfy.set_defaults(func=classify)

    # Remove Padding
    rm_pad = subparsers.add_parser("remove_padding")
    rm_pad.add_argument("image_path", type=valid_file, nargs='+',
                        help="path to original cxr image/s")
    rm_pad.add_argument("--outpath", type=valid_directory, required=False, default=None,
                        help="directory to save output image, default=None which overwrites the original image/s")
    rm_pad.add_argument("--bit_depth", required=False, default=None, type=int,
                        help="bit depth of image, default=None which follows the standard iio.imwrite behaviour")
    rm_pad.add_argument("--nworkers", required=False, default=1, type=int,
                        help="number of workers to use for multiprocessing, default=1")
    rm_pad.set_defaults(func=remove_padding)

    # Inpaint
    ipt = subparsers.add_parser("inpaint")
    ipt.add_argument("img_path", type=valid_file, nargs='+',
                     help="path to original cxr image/s")
    ipt.add_argument("--outpath", type=valid_directory, default="./inpainted",
                     help="directory to save output image, default='./inpainted'")
    ipt.add_argument("--method", required=False, default="inpaint_cxr", type=str,
                     help="method of removal or extract text: 'inpaint_cxr', 'bbox', or 'extract_text', "
                          "default='inpaint_cxr'")
    ipt.add_argument("--ll_mask", required=False, default=True, type=bool,
                     help="mask lower level text/annotation features, default=True")
    ipt.add_argument("--use_ns", required=False, default=False, type=bool,
                     help="use Navier-Stokes method rather than Telea 'Fast Marching' method (see cv.inpaint_cxr), "
                          "default=False")
    ipt.add_argument("--nworkers", required=False, default=1, type=int, )
    ipt.add_argument("--inversion_csv", required=False, default=None, type=valid_file,
                     help="csv file containing inversion information if using dcm, default=None")
    ipt.add_argument("--name_col", required=False, default=None, type=str,
                     help="column containing file names in the inversion_csv, "
                          "default=None")
    ipt.add_argument("--inversion_col", required=False, default=None, type=str,
                     help="column containing the inversion flag (0 or 1) in inversion_csv, "
                          "default=None")
    ipt.set_defaults(func=inpaint)

    # Inpaint comparison
    ipt_comparison = subparsers.add_parser("inpaint_comparison")
    ipt_comparison.add_argument("inpainted_path", type=valid_file, nargs='+',
                                help="path to inpainted cxr image/s")
    ipt_comparison.add_argument("--original_dir", type=valid_directory,
                                help="path to directory containing original cxr image/s (must have same name as "
                                     "inpainted image)")
    ipt_comparison.add_argument("--out_dir", type=str, default="./inpainted",
                                help="directory to save output image, default='./inpainted'")
    ipt_comparison.add_argument("--fig_size", required=False, default=(30, 10), type=tuple_type)
    ipt_comparison.add_argument("--title_size", required=False, default=25, type=int, )
    ipt_comparison.add_argument("--fig_title", required=False, default=None, type=str, )
    ipt_comparison.add_argument("--nworkers", required=False, default=1, type=int, )
    ipt_comparison.set_defaults(func=create_comparison_images)

    # OCR
    ocr = subparsers.add_parser("read_mask")
    ocr.add_argument("img_path", type=valid_file, nargs='+',
                     help="path/s to cxr annotation mask/s")
    ocr.add_argument("--outpath", type=str, default="./annotations.json",
                     help="path to save output json, default='./annotations.json'")
    ocr.add_argument("--nworkers", required=False, default=1, type=int, )
    ocr.add_argument("--method", required=False, default="combined", type=str,
                     help="method of text extraction: 'combined', 'python', or 'subprocess'")
    ocr.add_argument("--lang", required=False, default="eng", type=str,
                     help="language for pytesseract, default='eng'")
    ocr.add_argument("--words_file", required=False, default=None, type=valid_file,
                     help="path to file containing words to search for, default=None")
    ocr.add_argument("--patterns_file", required=False, default=None, type=valid_file,
                     help="path to file containing patterns to search for, default=None")
    ocr.set_defaults(func=read_masks)

    # Analyse quality metrics and create figures for development set
    qs_dev = subparsers.add_parser("quality_tuner")
    qs_dev.add_argument("quality_metric_file", type=valid_file, help="path to json containing the calculated quality "
                                                                     "metrics for the development set")
    qs_dev.add_argument("--outpath", type=valid_directory, default="./",
                        help="directory to save output json, default='./'")
    qs_dev.add_argument("--nworkers", required=False, default=1, type=int, )
    qs_dev.set_defaults(func=run_quality_tuner)

    # Quality scores
    qs = subparsers.add_parser("quality_scores")
    qs.add_argument("img_path", type=valid_file, nargs='+',
                    help="path to cxr image/s")
    qs.add_argument("--outpath", type=valid_directory, default="./",
                    help="directory to save output json, default='./'")
    qs.add_argument("--metrics", type=valid_metrics, default="all", nargs='+',
                    help="the quality metrics to calculate, default='all'")
    qs.add_argument("--nworkers", required=False, default=1, type=int, )
    qs.set_defaults(func=calculate_quality)

    # Run
    run = subparsers.add_parser("run", help="run the AutoQC pipeline")
    run.add_argument("--config", required=False, default=None, type=FileType("r"),
                     help="config file path - N.B. this will override other arguments")
    run.add_argument("--img_path", type=valid_file, required=False, nargs='+', help="path/s to cxr/s")
    run.add_argument("--outpath", type=str, required=False, default="./qc_outputs",
                     help="path to save output images, default='./qc_outputs'")
    run.add_argument("--checkpoint_dir", required=False, type=valid_directory,
                     help="checkpoint directory")
    run.add_argument("--json_dir", required=False, default=None, type=str,
                     help="directory of metadata jsons, default=None")
    run.add_argument("--cpu_workers", required=False, default=os.cpu_count(), type=int,
                     help="use multiple cpus, default=os.cpu_count()")
    run.add_argument("--dataset", required=False, default="", type=str,
                     help="dataset the images belong to, default=''")
    run.add_argument("--gpu", required=False, default=False, type=bool, help="use gpu, default=False")
    run.add_argument("--batch_size", required=False, default=8, type=int, help="batch size, default=8")
    run.add_argument("--luts_applied", required=False, default=False, type=bool,
                     help="luts already applied, default=False")
    run.add_argument("--exclude_or_review", required=False, default="review", type=str,
                     help="exclude or review outlier images, default=review")
    run.add_argument("--correct_inversion", required=False, default=True, type=bool,
                     help="correct grayscale inversion, default=True")
    run.add_argument("--correct_rotation", required=False, default=True, type=bool,
                     help="correct 90-degree rotation, default=True")
    run.add_argument("--autowindow_instead_of_luts", required=False, default=False, type=bool,
                     help="use autowindowing instead of luts in pixel transformations, default=False")
    run.add_argument("--autowindow_if_no_luts", required=False, default=True, type=bool,
                     help="use autowindowing if no luts are provided, default=True")
    run.add_argument("--save_stage2", required=False, default=True, type=bool,
                     help="save the stage 2 intermediate to avoid running text removal twice, default=True")
    run.add_argument("--keep_stage2", required=False, default=False, type=bool,
                     help="keep the stage 2 intermediate, default=False")
    run.add_argument("--log_level", required=False, default='info', type=str,
                     help="logging level, default='info'")
    run.add_argument("--log_file", required=False, default=None, type=str,
                     help="logging level, default='None'")
    run.add_argument("--save_progress", required=False, default=False, type=bool,
                     help="logging level, default='False'")
    run.add_argument("--progress_dir", required=False, default=None, type=valid_directory,
                     help="logging level, default=None")
    run.add_argument("--continue_progress", required=False, default=False, type=bool,
                     help="logging level, default='False'")
    run.add_argument("--keep_progress", required=False, default=False, type=bool,
                     help="logging level, default='False'")
    run.add_argument("--always_spawn", required=False, default=True, type=bool,
                     help="always initiate multiprocessing with 'spawn' rather than 'fork', default='True'")
    run.set_defaults(func=run_pipeline)

    # Combine figures
    combine_figs = subparsers.add_parser("combine_figs")
    combine_figs.add_argument("figures", type=str, nargs='+', help="paths to or directory of figures to combine")
    combine_figs.add_argument("--outpath", type=str, default="./combined_figures.png",
                              help="path to save output image, default='./combined_figures.png'")
    combine_figs.add_argument("--ncols", required=False, default=3, type=int)
    combine_figs.set_defaults(func=combine_figures_cl)

    # Data Explorer
    data_explorer = subparsers.add_parser("data_explorer")
    data_explorer.add_argument("--data", required=True, type=valid_file, nargs='+',
                               help="path/s to data file/s")
    data_explorer.add_argument("--join_on", required=False, default="name", type=str,
                               help="column to join on, default='name'")
    data_explorer.add_argument("--data_schema", required=False, default=None, type=valid_file,
                     help="path to user defined data schema, if None use default, default=None")
    data_explorer.add_argument("--value_maps", required=False, default=None, type=valid_file,
                     help="path to user defined value maps, if None use default, default=None")
    data_explorer.add_argument("--host", required=False, default='127.0.0.1', type=str,
                     help="host address, default='127.0.0.1'")
    data_explorer.add_argument("--port", required=False, default=5000, type=int,
                     help="port number, default=5000")
    data_explorer.add_argument("--cat_limit", required=False, default=15, type=int,
                     help="max number of categories when plotting with additional set to 'Other', default=15")
    data_explorer.add_argument("--all_dicom_headers", required=False, default=False, type=bool,
                     help="use all dicom headers for analysis that are in the schema, default=False")
    data_explorer.add_argument("--debug", required=False, default=False, type=bool,
                     help="run dash in dubug mode, default=False")
    data_explorer.set_defaults(func=run_data_explorer)

    args = parser.parse_args(input)
    if not hasattr(args, "func"):
        parser.print_help()

    return args


def main():
    """
    The main function that serves as the entry point for executing the commands.

    It parses the command line arguments and calls the corresponding function from the scripts module.
    If no function is associated with the parsed arguments (i.e., no command or an unrecognized command
    was given), the ArgumentParser's help message is printed.
    """
    args = parse_args(sys.argv[1:])

    if hasattr(args, "func"):
        args.func(args)
