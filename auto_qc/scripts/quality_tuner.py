"""
qual_dev.py: Script to run the quality metric pipeline for development.

This script runs the quality metric pipeline for development. It uses the quality metrics in the quality_metrics
directory and the images in the dev_img_folder directory. It outputs the results to the quality_results_dev.json file
and the figures to the quality_dev_figures directory.  These results can be utilised to determine which metrics and
sensitivities to use for the image quality step in the AutoQC pipeline.
"""

import os
import pandas as pd
import numpy as np
import imageio.v2 as iio
import json
from typing import Tuple, Union, Optional, List
from tqdm.auto import tqdm
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.figure import Figure, Axes
from matplotlib.pyplot import Line2D
from concurrent.futures import as_completed, ProcessPoolExecutor
from argparse import Namespace

from auto_qc.utils import bytescale
from auto_qc.operations.boundary import remove_border


def pad_image_to_square(image: np.ndarray, padding_color: int = 255) -> np.ndarray:
    """
    Pads an image to make it square.
    
    Parameters:
    - image: An image in the form of a numpy array.
    - padding_color: The color of the padding. Default is 255 (white).
    
    Returns:
    - A square image with padding.
    """
    
    height, width = image.shape[:2]

    # Identifying the difference in shape
    diff = abs(height - width)

    # Halving the difference and saving the ceiling and floor values
    pad1, pad2 = diff // 2, diff - diff // 2

    # Determine if height or width is smaller
    if height <= width:
        padding = ((pad1, pad2), (0, 0)) if len(image.shape) == 2 else ((pad1, pad2), (0, 0), (0, 0))
    else:
        padding = ((0, 0), (pad1, pad2)) if len(image.shape) == 2 else ((0, 0), (pad1, pad2), (0, 0))

    return np.pad(image, padding, mode='constant', constant_values=padding_color)


def plot_metric_examples(
        df,
        field: str,
        nrows: int = 10,
        layout_type: str = '6_cols',
        img_size: int = 4
) -> Tuple[Figure, Axes]:
    """
    Plots a grid of images with the lowest, middle and highest values of the given field.

    :param df: The dataframe containing the images and values.
    :param field: The quality metric to plot (column in the dataframe).
    :param nrows: The number of rows in the grid.
    :param layout_type: The layout of the grid. Either '3_cols' or '6_cols'.
    :param img_size: The size of the images in the grid.
    :return: A tuple containing the figure and axes.
    """

    if layout_type == '3_cols':
        ncols = 3
        line_cols = [0,1]
        title_cols = [0,1,2]
    else:
        ncols = 6
        line_cols = [1,3]
        title_cols = [0,2,4]

    # Calculate total grid size
    w_space=0.05
    h_space=0.1
    figsize = (img_size * ncols, img_size * nrows + 1)
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=figsize)
    plt.subplots_adjust(wspace=w_space, hspace=h_space)

    # Sorting dataframe by field
    df_sorted = df.sort_values(by=field).reset_index(drop=True)
    median_idx = int(len(df_sorted[field])/2)

    # Partitioning into lowest, middle, highest
    partition_size = int(nrows * ncols/3)
    low_images = df_sorted['image_path'][:partition_size].tolist()
    mid_images = df_sorted['image_path'][median_idx - partition_size//2 : median_idx + partition_size//2 + 1].tolist()
    high_images = df_sorted['image_path'][-partition_size:].tolist()
        
    low_idx, mid_idx, high_idx = 0, 0, 0

    for i, ax in enumerate(axes.flat):
        image_path = None
        if layout_type != '3_cols':
            if i % 6 < 2:
                if low_idx < len(low_images):
                    image_path = low_images[low_idx]
                    low_idx += 1
            elif 2 <= i % 6 < 4:
                if mid_idx < len(mid_images):
                    image_path = mid_images[mid_idx]
                    mid_idx += 1
            elif i % 6 >= 4:
                if high_idx < len(high_images):
                    image_path = high_images[high_idx]
                    high_idx += 1
        else:
            if i % 3 == 0:
                if low_idx < len(low_images):
                    image_path = low_images[low_idx]
                    low_idx += 1
            elif i % 3 == 1:
                if mid_idx < len(mid_images):
                    image_path = mid_images[mid_idx]
                    mid_idx += 1
            elif i % 3 == 2:
                if high_idx < len(high_images):
                    image_path = high_images[high_idx]
                    high_idx += 1
                    
        if image_path:
            image = iio.imread(image_path)
            image = bytescale(image)
            image = remove_border(image)
            image = pad_image_to_square(image)
            ax.imshow(image, cmap='gray')
            prob = df_sorted.loc[df_sorted['image_path'] == image_path, field].values[0]
            ax.set_title(f"{to_sentence_case(field)}: {df.loc[df.image_path == image_path, field].values[0]:.3f}")
            ax.axis('off')
            del image
    
    # Set Titles using the top row of axes
    col_titles = [f"Low {field}", f"Mid {field}", f"High {field}"]
    if layout_type != '3_cols':
        for col, title in zip(title_cols, col_titles):
            axes[0, col].text(1+w_space/2, 1+2*h_space, title, ha='center', va='top', fontsize=14, fontweight='bold', transform=axes[0, col].transAxes)
    else:
        for col, title in zip(title_cols, col_titles):
            axes[0, col].text(0.5, 1+2*h_space, title, ha='center', va='top', fontsize=14, fontweight='bold', transform=axes[0, col].transAxes)

    # Draw vertical lines using the top row of axes
    for col in line_cols:
        line_x = 1 + w_space / 2
        line_start = 1+2*h_space
        line_end = (1-h_space) - nrows - (h_space*(nrows-1))
        line = Line2D([line_x, line_x], [line_start, line_end], color='black', linewidth=2, transform=axes[0, col].transAxes, clip_on=False)
        axes[0, col].add_line(line)
        
    return fig, axes

def plot_removed_images_grid(
        df: pd.DataFrame,
        field: str,
        nrows: int = 5,
        ncols: int = 5,
        img_size: int = 4
) -> Tuple[Figure, Axes]:
    """
    Plots a grid of images removed due to quality.

    :param df: The dataframe containing the images and values.
    :param field: The quality metric to plot (column in the dataframe).
    :param nrows: The number of rows in the grid.
    :param ncols: The number of columns in the grid.
    :param img_size: The size of the images in the grid.
    :return: A tuple containing the figure and axes.
    """

    df_sampled = df.sample(n=nrows*ncols, replace=False)
    df_sampled = df_sampled.sort_values(by=field).reset_index(drop=True)
    
    figsize = (img_size * ncols, img_size * nrows)
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=figsize)
    plt.subplots_adjust(wspace=0.2, hspace=0.2)

    for i, ax in enumerate(axes.flat):
        if i < len(df_sampled):
            image_path = df_sampled.iloc[i]['image_path']
            image = iio.imread(image_path)
            image = bytescale(image)
            image = remove_border(image)
            image = pad_image_to_square(image)
            ax.imshow(image, cmap='gray')
            ax.set_title(f"{to_sentence_case(field)}: {df.loc[df.image_path == image_path, field].values[0]:.3f}")
            ax.axis('off')
            del image
        else:
            ax.axis('off')
    
    return fig, axes


def to_sentence_case(s: str) -> str:
    """
    Converts a string to sentence case.

    :param s: The string to convert.
    :return: The string in sentence case.
    """
    if not s:
        return ''
    return s[0].upper() + s[1:].lower()


def hist_plot(
        df: pd.DataFrame,
        field: str,
        ax: Optional[Axes] = None,
        timing: str = "Before",
        sensitivity: Optional[str] = None,
        legend: bool = True
) -> Union[Axes, Tuple[Figure, Axes]]:
    """
    Plots a histogram of the given field.

    :param df: The dataframe containing the images and values.
    :param field: The quality metric to plot (column in the dataframe).
    :param ax: The axes to plot on (optional).
    :param timing: The timing of the plot ('before' or 'after' removal of outliers).
    :param sensitivity: The sensitivity of the plot (high, normal or low).
    :param legend: Whether to include a legend.
    :return: The axes or a tuple containing the figure and axes if no axes was provided.
    """
    new_fig = False
    if ax is None:
        fig, ax = plt.subplots()
        new_fig = True
    
    unique_categories = df['dataset'].unique()

    for category in unique_categories:
        subset = df[df['dataset'] == category]
        sns.histplot(subset[field], kde=False, 
                     # stat="density", 
                     label=category, alpha=0.5, ax=ax)

    if legend:
        ax.legend(prop={'size': 10}, title='Category')
    if not sensitivity or timing.lower() == "before":
        ax.set_title(f'{to_sentence_case(field)} - {timing}')
    else:
        ax.set_title(f'{to_sentence_case(field)} - {timing} ({sensitivity} sens.)')
    ax.set_xlabel(to_sentence_case(field))
    ax.set_ylabel('Frequency')
    if new_fig:
        return fig, ax
    else:
        return ax


def remove_outliers_using_MAD(
        df: pd.DataFrame,
        field: str,
        tolerance: float,
        partition_to_remove: str = "both"
) -> Tuple[pd.DataFrame, pd.DataFrame, Tuple[float, float]]:
    """
    Removes outliers from a dataframe using the Median Absolute Deviation (MAD) method.

    :param df: The dataframe containing the images and values.
    :param field: The quality metric to use for outlier detection.
    :param tolerance: The 'tolerance' to use for outlier detection, which sets the sensitivity of the method.
    :param partition_to_remove: The partition of outliers to remove from. Any of 'both', 'upper' or 'lower'. Default is
        'both'.
    :return: The filtered dataframe, the removed dataframe and the lower and upper bounds.
    """

    conditions = []
    column = df[field]
    median = column.median(skipna=True)
    MAD = np.median((np.abs(column - median)).dropna())

    outlier_fence_low = median - (tolerance * MAD)
    outlier_fence_high = median + (tolerance * MAD)

    if partition_to_remove == "both":
        condition = (((df[field] >= outlier_fence_low) & (df[field] <= outlier_fence_high)) | df[field].isna())
    elif partition_to_remove == "upper":
        condition = ((df[field] <= outlier_fence_high) | df[field].isna())
    elif partition_to_remove == "lower":
        condition = ((df[field] >= outlier_fence_low) | df[field].isna())
    else: 
        raise ValueError(f"Invalid partition_to_remove entered ({partition_to_remove}), "
                         f"please enter one of 'both', 'upper', or 'lower'.")
    conditions.append(condition)

    combined_condition = pd.concat(conditions, axis=1).all(axis=1)
    filtered_df = df[combined_condition]
    removed_df = df[~combined_condition]
    
    return filtered_df, removed_df, (outlier_fence_low, outlier_fence_high)


def minimise_objective(data: pd.DataFrame, field: str, lambda_: float = 1.0) -> Tuple[float, float]:
    """
    Minimises the objective function for the MAD method.

    :param data: The dataframe containing the images and values.
    :param field: The quality metric to use for outlier detection.
    :param lambda_: The lambda value to use for the objective function.
    :return: The best constant and the removal rate.
    """
            
    constants = [i*0.1 for i in range(10, 51)]  # [1, 1.1, 1.2, ..., 5]

    best_objective = np.inf
    best_constant = None
    best_removal_rate = None

    for constant in constants:
        cleaned_data, _, _ = remove_outliers_using_MAD(data, field, constant)
        removal_rate = (len(data) - len(cleaned_data)) / len(data)
        std_dev = np.std(cleaned_data[field])

        # Objective function
        if std_dev != 0:
            objective = std_dev + lambda_ * removal_rate

            if objective < best_objective:
                best_objective = objective
                best_constant = constant
                best_removal_rate = removal_rate
    
    return best_constant, best_removal_rate


def get_metric_stats(df: pd.DataFrame, field: str) -> dict:
    """
    Get statistics for a given quality metric.

    :param df: The dataframe containing the images and quality values.
    :param field: The quality metric to get statistics for.
    :return: A dictionary of the statistics for the given quality metric.
    """
    stats = {
        "overall": {
            "n": len(df),
            "mean": df[field].mean(skipna=True),
            "median": df[field].median(skipna=True),
            "stddev": df[field].std(skipna=True),
            "min": df[field].min(skipna=True),
            "max": df[field].max(skipna=True)
        }
    }

    for dataset in df.dataset.unique():
        stats.update(
            {
                dataset: {
                    "n": len(df),
                    "mean": df[df.dataset == dataset][field].mean(skipna=True),
                    "median": df[df.dataset == dataset][field].median(skipna=True),
                    "stddev": df[df.dataset == dataset][field].std(skipna=True),
                    "min": df[df.dataset == dataset][field].min(skipna=True),
                    "max": df[df.dataset == dataset][field].max(skipna=True)
                }
            }
        )
    return stats


def quality_dev_metric(
        df: pd.DataFrame,
        field: str,
        sensitivity: Union[str, float],
        partition_to_remove: str = 'both',
        fig_cols: int = 6,
        fig_rows: int = 4
) -> Tuple[pd.DataFrame, pd.DataFrame, dict, dict]:
    """
    Runs the quality development metric pipeline. We attempt to minimise the std dev whilst minimising the
    number removed using the MAD method. We then remove outliers using the best constant and plot the results.

    :param df: The dataframe containing the images and quality values.
    :param field: The quality metric to use for outlier detection.
    :param sensitivity: The sensitivity of the MAD method. Either 'high', 'normal' or 'low', or a float value.
    :param partition_to_remove: The partition of outliers to remove from. Any of 'both', 'upper' or 'lower'. Default is
        'both'.
    :param fig_cols: The number of columns in the figures.
    :param fig_rows: The number of rows in the figures.
    :return: The cleaned dataframe, the removed dataframe, the results dictionary and the figures dictionary.
    """
        
    if isinstance(sensitivity, str):
        if sensitivity == "high":
            sensitivity = 1.5
        elif sensitivity == "low":
            sensitivity = 3.5
        else:
            sensitivity = 2.5
            
    if sensitivity == 1:
        sens_string = "High"
    elif sensitivity == 3:
        sens_string = "Low"
    else:
        sens_string = "Normal"
    
    # Before statistics
    results = {
        "before": get_metric_stats(df, field)
    }
    
    # Before histogram
    hists, ax = plt.subplots(1, 2, figsize=(12,6), sharey=True)
    ax[0] = hist_plot(df, field, timing="Before", legend=False, ax=ax[0], sensitivity=sens_string)

    # Attempt to minimise std dev whilst minimise number removed
    initial_std_dev = df[field].std()
    lambda_ = sensitivity * initial_std_dev
    results['best_constant'], results['removal_rate'] = minimise_objective(df, field, lambda_=lambda_)
    
    # Get outputs
    cleaned, removed, results["bounds"] = remove_outliers_using_MAD(df, field, results['best_constant'], partition_to_remove)
    
    # After statistics
    results["after"] = get_metric_stats(cleaned, field)

    # After histogram
    ax[1] = hist_plot(cleaned, field, timing="After", legend=True, ax=ax[1], sensitivity=sens_string)
    
    # Generate examples figure
    if fig_cols >=6:
        ex_fig, _ = plot_metric_examples(df, field, nrows = fig_rows, layout_type = '6_cols')
    else:
        ex_fig, _ = plot_metric_examples(df, field, nrows = fig_rows, layout_type = '3_cols')
    
    # Generate grid of removed images
    if len(removed) > 0:
        if len(removed) >= fig_cols:
            rm_fig, _ = plot_removed_images_grid(removed, field, ncols=fig_cols, nrows=fig_rows)
        else:
            rm_fig, _ = plot_removed_images_grid(removed, field, ncols=len(removed), nrows=fig_rows)
        rm_fig.tight_layout()
    else:
        print("No images removed with selected partition_to_remove...")
        rm_fig = None
        
    # Create figure dict.
    figs = {
        'hists': hists, 
        'examples': ex_fig, 
        'removed': rm_fig
    }
    
    return cleaned, removed, results, figs


def get_correlation_matrix(df: pd.DataFrame, metrics: List[str]) -> Tuple[Figure, Axes]:
    """
    Get the correlation matrix for the given dataframe.

    :param df: The dataframe containing the images and quality values.
    :param metrics: The quality metrics to use for the correlation matrix.
    :return: The correlation matrix fig and axes.
    """
    corr = df[metrics].corr()
    corr_matrix, corr_matrix_ax = plt.subplots(figsize=(10, 10))
    sns.heatmap(corr, annot=True, cmap='coolwarm', center=0, ax=corr_matrix_ax)
    labels = corr_matrix_ax.get_xticklabels()
    corr_matrix_ax.set_xticklabels(labels, rotation=45, ha='right')
    corr_matrix.tight_layout()
    return corr_matrix, corr_matrix_ax


def process_metric_sensitivity(
        df: pd.DataFrame,
        metric: str,
        sensitivity: str,
        outpath: Optional[str] = "./",
) -> Tuple[str, str, dict]:
    """
    Process a given metric and outlier sensitivity.

    :param df: The dataframe containing the images and quality values.
    :param metric: The quality metric to use.
    :param sensitivity: The sensitivity of the MAD method. Either 'high', 'normal' or 'low', or a float value.
    :param outpath: The output path to save the figures to, if save_figs is True. Optional. Default is './'.
    :return: The metric, sensitivity and results.
    """

    _, _, res, figs = quality_dev_metric(df, metric, sensitivity, fig_cols=6, fig_rows=3)
    figs['hists'].savefig(os.path.join(outpath, "quality_dev_figures", f"{metric}_{sensitivity}_hists_dev.png"), bbox_inches='tight', pad_inches=0)
    figs['examples'].savefig(os.path.join(outpath, "quality_dev_figures", f"{metric}_{sensitivity}_exmpls_dev.png"), bbox_inches='tight', pad_inches=0)
    figs['removed'].savefig(os.path.join(outpath, "quality_dev_figures", f"{metric}_{sensitivity}_rmvd_exmpls_dev.png"), bbox_inches='tight', pad_inches=0)
    plt.close('all')
    return metric, sensitivity, res


def make_corr_matrix(df: pd.DataFrame, metrics: List[str], outpath: str):
    """
    Make the correlation matrix and save in outpath.

    :param df: The dataframe containing the images and quality values.
    :param metrics: The quality metrics to use for the correlation matrix.
    :param outpath: The output path to save the figures to, if save_figs is True. Optional. Default is './'.
    """
    correlation_matrix, _ = get_correlation_matrix(df, metrics)
    os.makedirs(os.path.join(outpath, "quality_dev_figures"), exist_ok=True)
    correlation_matrix.savefig(os.path.join(outpath, "quality_dev_figures", f"correlation_matrix.png"),
                               bbox_inches='tight', pad_inches=0)
    plt.close(correlation_matrix)
    del correlation_matrix, _


def created_combined_metrics(metrics: List[str], df: pd.DataFrame) -> Tuple[List[str], pd.DataFrame]:
    """
    Create a combined metrics of the full and cutout50 metrics.

    :param metrics: The list of metrics.
    :param df: The dataframe containing the images and quality values.
    :return: The updated metrics list and dataframe.
    """
    to_combine = [
        'niqe',
        'brisque',
        'contrast_ratio',
        'entropy',
        'sharpness',
        'skewness',
    ]

    for metric in to_combine:
        if metric and f"{metric}_cutout50" in metrics:
            df[f"{metric}_combined"] = df[f"{metric}"] + df[f"{metric}_cutout50"]
            metrics.append(f"{metric}_combined")
    return metrics, df


def read_json(json_path: str) -> dict:
    """
    Read in a json file.

    :param json_path: The path to the json file.
    :return: The data dictionary.
    """
    with open(json_path, "r") as f:
        data_dict = json.load(f)
    return data_dict


def save_json(outp: str, outdata: dict):
    """
    Save a dictionary to a json file.

    :param outp: The output path.
    :param outdata: The data to save.
    """
    with open(os.path.join(outp, "quality_results_dev.json"), "w") as f:
        json.dump(outdata, f)


def run_quality_tuner(args: Namespace):
    """
    Run the quality development pipeline.

    :param args: The arguments.
    """
    # Initialise variables
    quality_metric_values = args.quality_metric_file
    sensitivities = ['low', 'normal', 'high']
    executor = None
    outpath = args.outpath
    nworkers = args.nworkers

    # Set matplotlib to not warn about too many open figures
    matplotlib.rcParams['figure.max_open_warning'] = nworkers * 2

    # Read in data and convert to dataframe
    data_dict = read_json(quality_metric_values)
    df = pd.DataFrame(data_dict).transpose()

    # Get metrics list
    metrics = list(df.columns)
    # Create combined metrics of full and cutout50

    metrics, df = created_combined_metrics(metrics, df)

    # Add dataset column
    df['dataset'] = df.index.str.split("_").str[0]

    # Create correlation matrix
    make_corr_matrix(df, metrics, outpath)

    # Run quality metric development pipeline
    out = {metric: {} for metric in metrics}
    try:
         with ProcessPoolExecutor(max_workers=nworkers) as executor:
            futures = [
                executor.submit(
                    process_metric_sensitivity, df, metric, sensitivity, outpath,
                ) for metric in metrics for sensitivity in sensitivities
            ]
            for future in tqdm(as_completed(futures), total=len(futures)):
                metric, sensitivity, result = future.result()
                out[metric][sensitivity] = result
    except KeyboardInterrupt:
        print('KeyboardInterrupt: Stopping...')
    except Exception as e:
        print(e)
    finally:
        if executor is not None:
            executor.shutdown(wait=True)

    save_json(outpath, out)
