"""
classify.py: Classify Images Using an AutoQC Tool.

The classify.py module provides functionalities for image classification based on specified classification types. It
employs Dask for distributed computing and uses image reading and batch processing utilities. It is not used for
classification based on the deep learning models.

Main Components:

- read_image: Reads an image file and returns the image data.
- classify: Reads a batch of images and classifies them based on a specified type such as 'boundary' or 'quality'. It
uses different classification functions depending on the classification type.

This module is particularly useful for batch classification tasks of images, offering flexibility in terms of the type
of classification required. It provides two types of classifications: boundary-based and quality-based, and the
classification type can be specified through command-line arguments.
"""

from argparse import Namespace

import dask

from ..classification import classify_boundary, classify_boundary2, contrast_ratio
from ..utils import batcher, imread


@dask.delayed
def read_image(imgname: str) -> object:
    """
    Read an image file and return it.

    :param imgname: Name of the image file to read
    :return: Image data
    """
    img = imread(imgname)
    return img


def classify(args: Namespace) -> None:
    """
    Read a batch of images and classify them based on the type specified in command line arguments.

    :param args: Command line arguments, should contain 'imglist', 'batch', and 'classtype'
    """
    for imgs in batcher(args.imglist, args.batch):
        img = [read_image(name) for name in imgs]
        img = dask.compute(img)[0]
        if args.classtype in ["boundary"]:
            cls = [classify_boundary(im) for im in img]
            clsc = [classify_boundary2(im) for im in img]
        elif args.classtype in ["quality"]:
            cls = [contrast_ratio(im) for im in img]
            clsc = [contrast_ratio(im, cutout=True) for im in img]
        for i, name in enumerate(imgs):
            print(f"{name} {cls[i]:.2f} {clsc[i]:.2f}")
