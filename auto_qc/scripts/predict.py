"""
predict.py: Predict Labels for a Batch of Images

The predict.py module provides functionalities to predict labels for a batch of images using a trained model. It
relies on Dask for efficient computation and uses the preprocess and serve modules to preprocess the images and load the
trained model, respectively.

Main Components:

- read_image: Reads an image file, preprocesses the image, and returns it as a numpy array.
- predict: Loads a trained model and uses it to predict labels for a batch of images.

The module is particularly useful when there's a need to make predictions on a large set of images using a trained
model. The batch processing mechanism allows for efficient and faster computations.
"""

from argparse import Namespace

import dask
import numpy as np

from ..utils import batcher, imread
from .preprocess import preprocess_image
from .serve import load_model


@dask.delayed
def read_image(imgname: str) -> np.array:
    """
    Read an image file, preprocess the image, and return it as a numpy array.

    :param imgname: Name of the image file to read and preprocess
        :return: Preprocessed image data
    """
    img = imread(imgname)
    img = preprocess_image(img)
    return img


def predict(args: Namespace) -> None:
    """
    Load a trained model and use it to predict labels for a batch of images.

    :param args: Command line arguments, should contain 'model', 'imglist', and 'batch'
        """
    model = load_model(args.model)
    for imgs in batcher(args.imglist, args.batch):
        img = [read_image(name) for name in imgs]
        img = dask.compute(img)[0]
        img = np.stack(img)
        pred = model.predict(img[..., None])
        for i, name in enumerate(imgs):
            print(name, pred[i][0])
    return
