from typing import List, Union

import numpy as np


Array2D = Union[List[List[float]], np.ndarray]


def classify_boundary(img: Array2D) -> float:
    """
    Calculate the amount of blank space around an image.

    :param img: Image array to classify

    :return: Percentage of blank space around image.
    """
    ysize, xsize = img.shape
    npix = ysize * xsize
    img = img - img.min()
    p0 = (npix - np.count_nonzero(img)) / npix
    return p0


def classify_boundary2(img: Array2D) -> float:
    """
    Compute the amount of blank space around an image using a more complex algorithm.

    This function performs the calculation based on the median value of image intensities
    and takes into account the border pixels. The image is normalized and the number of
    pixels in the blank space are counted and converted to a percentage.

    :param img: Image array to classify

    :return: Percentage of blank space around image.
    """
    img_min = img.min()
    img = img - img_min
    val = np.median(img, axis=1).min() * 0.0
    x = (np.median(img, axis=1, keepdims=True) > val) * 1
    x = np.resize(x, (img.shape[1], img.shape[0])).T

    val = np.median(img, axis=0).min() * 0.0
    y = (np.median(img, axis=0, keepdims=True) > val) * 1
    y = np.resize(y, (img.shape[0], img.shape[1]))

    nimg = img * x * y
    cimg = (img * 0 + 100) * x * y

    ysize, xsize = cimg.shape
    npix = ysize * xsize
    p0 = (npix - np.count_nonzero(cimg)) / npix
    return p0

