"""
aspect_ratio.py

This module performs image classification using aspect ratio-based methods. It is intended to be used as part of the
AutoQC pipeline, aiming to remove images that only contain a limited fraction of the lungs with a high specificity and
negative predictive value. It contains utility functions for reading, processing, and classifying images, as well as
plotting histograms, confusion matrices, and more.

Functions:
- calc_aspect_ratio: Calculate the aspect ratio of an image or a DataFrame of images.
- get_shapes_and_ratio: Calculate shapes and aspect ratios for a list of images.
- read_label_file: Read the label file for a given dataset.
- load_labels_and_aspect_ratios: Get aspect ratios for a specified dataset.
- allocate_gt: Allocate ground truth labels based on the given fractions.
- plot_histogram: Plot histogram of aspect ratios for two groups of images.
- classify_images: Classify images based on their aspect ratios and given bounds.
- calc_weighted_score: Calculate a weighted score based on sensitivity and specificity.
- plot_bounds_histogram: Plot histogram with annotated lower and upper bounds.
- plot_results_by_weight: Plot multiple histograms of results for various specificity weights between 1 and 2.
- find_best_bounds: Find the best lower and upper bounds for classification based on a given specificity weight.
- find_best_bounds_by_weight: Find the best lower and upper bounds for classification for each specificity weight.
- conf_matrix: Plot the confusion matrix for the given DataFrame.
- get_metrics: Calculate the classification metrics for the given DataFrame.
- results_by_missing_lung: Aggregate results by the fraction of missing lung.
"""

# IMPORTS --------------------------------------------------------------------------------------------------------------

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
from mlxtend.plotting import plot_confusion_matrix
import itertools
import math
from auto_qc.operations.boundary import remove_border
from auto_qc.utils import imread
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import Pool
from tqdm.auto import tqdm
from typing import Union, List, Dict, Tuple, Optional, Any, Sequence

# FIGURE SETTINGS ------------------------------------------------------------------------------------------------------

plt.rcParams.update({'font.size': 14})

CONFUSION_MATRIX_FIGSIZE = (6, 6)

COLORMAP = "Set3"
STACKED_ALPHA = 0.5
EDGE_ALPHA = 1

edgecolor = (0, 0, 0, EDGE_ALPHA)
cmap = plt.get_cmap(COLORMAP)

num_colors = cmap.N  # Total number of colors in the colormap
colors = cmap(np.arange(num_colors))
colors[1] = np.array([212 / 255, 175 / 255, 55 / 255, 1])  # Change the second color to darker yellow

color_dict = {
    'train': colors[9],  # purple
    'internal_train': colors[9],
    'val': colors[6],  # green
    'internal_val': colors[6],
    'internal': colors[1],  # dark yellow
    'internal_test': colors[1],
    'all_external': colors[8],  # grey
    'external': colors[8],
    'cuh': colors[3],  # red
    'brixia': colors[4],  # blue
    'ricord': colors[5],  # orange
    'mimic': colors[7],  # pink
    '0': colors[10],  # brown
    0: colors[10],
    '1': colors[2],  # light blue
    1: colors[2],
}

# Make confusion matrix colors correspond to main colours
CONFUSION_MATRIX_COLORS = {
    'train': 'Purples',  # yellow / brown
    'Training set': 'Purples',
    'internal_train': 'Purples',
    'val': 'Greens',  # green
    'Validation set': 'Greens',
    'internal_val': 'Greens',
    'internal': 'YlOrBr',  # purples
    'internal_test': 'YlOrBr',
    'Internal Test': 'YlOrBr',
    'Test set': 'YlOrBr',
    'all_external': 'Greys',  # greys
    'All External': 'Greys',
    'external': 'Greys',
    'cuh': 'Reds',  # red
    'CUH': 'Reds',
    'brixia': 'Blues',  # blue
    'Brixia': 'Blues',
    'ricord': 'Oranges',  # oranges
    'RICORD-1C': 'Oranges',
    'mimic': 'RdPu',  # pink
    'MIMIC': 'RdPu',
}

stacked_colors = [
    np.append(colors[10][0:3], STACKED_ALPHA, ),
    np.append(colors[2][0:3], STACKED_ALPHA, ),
]

# FUNCTIONS ------------------------------------------------------------------------------------------------------------

def calc_aspect_ratio(
        x_shape: float = None, y_shape: float = None, df: pd.DataFrame = None
) -> Union[float, pd.DataFrame]:
    """
    Calculate the aspect ratio of an image or add a new 'aspect_ratio' column to a DataFrame.

    :param x_shape: The x-dimension of the image, defaults to None.
    :param y_shape: The y-dimension of the image, defaults to None.
    :param df: DataFrame containing 'x_shape' and 'y_shape' columns, defaults to None.
    :return: The aspect ratio (x_shape / y_shape) if x_shape and y_shape are provided, or the input DataFrame with a
        new 'aspect_ratio' column if df is provided.
    :raise ValueError: If neither a dataframe nor x_shape and y_shape are provided.
    """
    if x_shape is not None and y_shape is not None:
        return x_shape / y_shape
    elif df is not None:
        df['aspect_ratio'] = df['x_shape'] / df['y_shape']
        return df
    else:
        raise ValueError('Must provide either a dataframe or x_shape and y_shape')


def _process_image(row: pd.Series, path_col: str = 'path') -> Tuple[str, int, int, float]:
    """
    Process a single image, removing the border and calculating its aspect ratio.

    :param row: A pandas Series containing the image information.
    :param path_col: The column name that holds the image path in the input row, defaults to 'path'.
    :return: A tuple containing the image path, y_shape, x_shape, and aspect_ratio.
    """
    img = imread(row[path_col])
    cropped = remove_border(img)
    y_shape, x_shape = cropped.shape
    aspect_ratio = calc_aspect_ratio(x_shape, y_shape)
    return row[path_col], y_shape, x_shape, aspect_ratio


def get_shapes_and_ratio(img_list: List[str]) -> pd.DataFrame:
    """
    Calculate the shapes and aspect ratios of images in the given list.

    :param img_list: A list of image file paths.
    :return: A DataFrame containing 'path', 'y_shape', 'x_shape', and 'aspect_ratio' columns.
    """
    try:
        with ThreadPoolExecutor() as executor:
            futures = [executor.submit(_process_image, img, 'path') for img in img_list]

            data = []
            for future in tqdm(futures, total=len(futures)):
                result = future.result()
                data.append(result)
    except Exception as e:
        print(e)
    finally:
        if executor is not None:
            executor.shutdown(wait=True)

    return pd.DataFrame(data, columns=['path', 'y_shape', 'x_shape', 'aspect_ratio'])


def read_label_file(dataset: str, dev_labels_dir: Optional[str] = None,
                    ext_labels_file: Optional[str] = None) -> pd.DataFrame:
    """
    Read the label file for the specified dataset.

    :param ext_labels_file: The path to the external test labels file.
    :param dataset: The name of the dataset ('internal' or 'external').
    :param dev_labels_dir: The directory containing the label file.
    :return: A DataFrame containing the label file data.
    :raise ValueError: If an invalid dataset name is provided.
    """
    if 'internal' in dataset:
        df = pd.read_csv(os.path.join(dev_labels_dir, 'stage2_paths+labels.csv'))
    elif dataset == 'external':
        df = pd.read_csv(ext_labels_file)
    else:
        raise ValueError(f'Invalid dataset: {dataset}')
    return df


def get_aspect_ratios(df, dataset_group: str = 'internal_dev', shapes_dir: str = "./") -> pd.DataFrame:
    """
    Get the aspect ratios of images in the specified dataset group (internal_dev, internal_test, external). If it
    exists, it loads the shapes file. If the aspect ratio is not provided with the shapes, it is calculated. If the
    shapes file does not exist, the shapes are measured and the aspect ratios calculated.

    :param df: Dataframe containing the labels, and the image paths and ds_names.
    :param dataset_group: The group the dataset belongs to (internal_dev, internal_test, external).
    :param shapes_dir: The directory in which the shapes file is stored. Defaults to "./".
    :return: The original dataframe with the shapes and aspect ratios added.
    """
    if 'aspect_ratio' in df.columns:
        return df.drop(columns=[col for col in df.columns if 'Unnamed' in col])

    shapes_file = os.path.join(shapes_dir, f'{dataset_group.split("_")[0]}_image_shapes.csv')
    if not os.path.exists(shapes_file):
        img_list = df['path'].tolist()
        shapes = get_shapes_and_ratio(img_list)
    else:
        shapes = pd.read_csv(shapes_file)
        if "name" not in shapes.columns:
            shapes['name'] = shapes['path'].apply(os.path.basename)
        shapes = shapes.drop('path', axis=1)
        if 'aspect_ratio' not in shapes.columns:
            shapes = calc_aspect_ratio(df=shapes)

    df_ = df.merge(shapes, how='left', on='name')
    return df_.drop(columns=[col for col in df_.columns if 'Unnamed' in col])


def load_labels_and_aspect_ratios(
        dataset: str = 'internal_dev', shapes_dir: str = "./", labels_dir: str = "./"
) -> pd.DataFrame:
    """
    Get the aspect ratios of images in the specified dataset and the associated labels. Reads in the labels from the
    label file and merges them with the shapes and aspect ratios from the shapes file. If the aspect ratios are not
    provided in the shapes file, they are calculated. If the shapes file does not exist, the shapes are measured, and
    the aspect ratios calculated.

    :param dataset: The name of the dataset ('internal_dev', 'internal_test', or 'external'). Defaults to
        'internal_dev'.
    :param shapes_dir: The directory containing the shapes file. Defaults to './'.
    :param labels_dir: The directory containing the label file. Defaults to './'.
    :return: A DataFrame containing image aspect ratios and other relevant data.
    :raise ValueError: If an invalid dataset name is provided.
    """

    if dataset == 'internal_dev':
        df = read_label_file(dataset, dev_labels_dir=labels_dir)
        df = df[
            ((df.partition == 'train') | (df.partition == 'val')) &
            (df.fraction.notna()) & (df.fraction != 'lateral')
            ].reset_index(drop=True)
    elif dataset == 'internal_test':
        df = read_label_file(dataset, dev_labels_dir=labels_dir)
        df = df[(df.partition == 'test') & (df.fraction.notna()) & (df.fraction != 'lateral')].reset_index(drop=True)
    elif dataset == 'external':
        df = read_label_file(dataset, ext_labels_file=labels_dir)
        df = df[df.fraction.notna() & (df.fraction != 'lateral')].reset_index(drop=True)
        df['path'] = df['stage2_path']
    else:
        raise ValueError(f"Invalid dataset: {dataset}. Please use 'internal_dev', 'internal_test', or 'external'.")

    df.path = df.path.apply(lambda x: os.path.expanduser(x))

    return get_aspect_ratios(df, dataset, shapes_dir)


def allocate_gt(df: pd.DataFrame) -> pd.DataFrame:
    """
    Allocate ground truth labels to the given DataFrame.

    :param df: The input DataFrame containing 'fraction' column.
    :return: The input DataFrame with an additional 'gt' column containing ground truth labels.
    """
    df.loc[(df['fraction'] == '100%') | (df['fraction'] == '66-99%'), 'gt'] = 0
    df.loc[(df['fraction'] != '100%') & (df['fraction'] != '66-99%'), 'gt'] = 1
    return df


def plot_histogram(
        df: pd.DataFrame, ax=None, legend=True, rwidth: float = 0.8, bins: int = 20, **kwargs
) -> Union[plt.Axes, Tuple[plt.Figure, plt.Axes]]:
    """
    Plot a histogram of aspect ratios for each ground truth group.

    :param df: The input DataFrame containing 'gt' and 'aspect_ratio' columns.
    :param ax: The Axes object to draw the plot on, defaults to None.
    :param legend: Whether to display a legend on the plot, defaults to True.
    :param rwidth: The relative width of the bars as a fraction of the bin width, defaults to 0.8.
    :param bins: The number of bins to use, defaults to 20.
    :param kwargs: Additional keyword arguments to pass to the Axes.hist() method.
    :return: The Axes object containing the plot, or a tuple (Figure, Axes) if a new figure is created.
    """
    fig_obj = None
    if ax is None:
        fig_obj, ax = plt.subplots(figsize=(8, 6))

    group1_data = df[df['gt'] == 0]['aspect_ratio']
    group2_data = df[df['gt'] == 1]['aspect_ratio']

    # Determine the common bin edges for both groups
    data_min = min(group1_data.min(), group2_data.min())
    data_max = max(group1_data.max(), group2_data.max())
    bin_edges = np.linspace(data_min, data_max, bins + 1)

    ax.hist(group1_data, density=True, label='0', color=stacked_colors[0],
            rwidth=rwidth, bins=bin_edges, edgecolor=edgecolor, linewidth=1, **kwargs)
    ax.hist(group2_data, density=True, label='1', color=stacked_colors[1],
            rwidth=rwidth, bins=bin_edges, edgecolor=edgecolor, linewidth=1, **kwargs)

    ax.set_xlabel('Aspect Ratio')
    ax.set_ylabel('Density')
    if legend:
        ax.legend(loc='upper right')

    return ax if fig_obj is None else (fig_obj, ax)


def classify_images(df: pd.DataFrame, lb: float, ub: float) -> pd.DataFrame:
    """
    Classify images based on their aspect ratios and given lower and upper bounds.

    :param df: The input DataFrame containing 'aspect_ratio' column.
    :param lb: The lower bound for classification.
    :param ub: The upper bound for classification.
    :return: The input DataFrame with an additional 'pred' column containing predicted labels.
    """
    df.aspect_ratio = df.aspect_ratio.astype(float)
    df.loc[(df.aspect_ratio > ub) | (df.aspect_ratio < lb), "pred"] = 1
    df.loc[(df.aspect_ratio < ub) & (df.aspect_ratio > lb), "pred"] = 0
    return df


def calc_weighted_score(
        sensitivity: float, specificity: float, sensitivity_weight: float = 1, specificity_weight: float = 1.75
) -> float:
    """
    Calculate a weighted score based on sensitivity and specificity.

    :param sensitivity: The sensitivity value.
    :param specificity: The specificity value.
    :param sensitivity_weight: The weight for sensitivity, defaults to 1.
    :param specificity_weight: The weight for specificity, defaults to 1.75.
    :return: The weighted score.
    """
    return specificity_weight * specificity + sensitivity_weight * sensitivity


def plot_bounds_histogram(
        df: pd.DataFrame, bounds: Sequence[float], ax=None, legend=True
) -> Union[plt.Axes, Tuple[plt.Figure, plt.Axes]]:
    """
    Plot a histogram of aspect ratios for each ground truth group with vertical lines indicating the lower and upper
    bounds.

    :param df: The input DataFrame containing 'gt' (ground truth) and 'aspect_ratio' columns.
    :param bounds: A tuple containing the lower and upper bounds.
    :param ax: The Axes object to draw the plot on, defaults to None.
    :param legend: Whether to display a legend on the plot, defaults to True.
    :return: The Axes object containing the plot, or a tuple (Figure, Axes) if a new figure is created.
    """
    fig_obj = None
    if ax is None:
        fig_obj, ax = plt.subplots(figsize=(8, 6))

    ax = plot_histogram(df, ax, legend)
    ax.axvline(bounds[0], color='black', linestyle='--', label=f'Lower B.')
    ax.axvline(bounds[1], color='black', linestyle='-.', label=f'Upper B.')

    if legend:
        gt_0_handle = Line2D([0], [0], marker='o', color='w',
                             markerfacecolor=np.append(stacked_colors[0][0:3], 1), label='>=66% lungs')
        gt_1_handle = Line2D([0], [0], marker='o', color='w',
                             markerfacecolor=np.append(stacked_colors[1][0:3], 1), label='<66% lungs')
        lower_bound_handle = Line2D([0], [0], color='black', linestyle='--', lw=2, label='Lower B.')
        upper_bound_handle = Line2D([0], [0], color='black', linestyle='-.', lw=2, label='Upper B.')
        ax.legend(handles=[gt_0_handle, gt_1_handle, lower_bound_handle, upper_bound_handle])

    return ax if fig_obj is None else (fig_obj, ax)


def plot_results_by_weight(
        df: pd.DataFrame, results: Dict[float, Dict[str, Any]], cols: int = 3, legend: bool = False
) -> Tuple[plt.Figure, np.ndarray]:
    """
    Plot the results by the specificity weight.

    :param df: The input DataFrame containing 'gt' and 'aspect_ratio' columns.
    :param results: The results dictionary containing specificity weights as keys and corresponding result data.
    :param cols: The number of columns in the grid of plots, defaults to 3.
    :param legend: Whether to display a legend on the plot, defaults to False.
    :return: A tuple containing the Figure and an array of Axes objects.
    """
    rows = math.ceil(len(results) / 3)
    fig, axs = plt.subplots(rows, cols, figsize=(4 * cols, 4 * rows))

    for i, specificity_weight in enumerate(results.keys()):
        j = math.floor(i / rows)
        k = i - (rows * j)
        plot_bounds_histogram(df, results[specificity_weight]["bounds"], axs[k, j], legend=legend)
        axs[k, j].set_title(f'{specificity_weight:.2f}, '
                            f'Sens: {results[specificity_weight]["metrics"]["sens"]:.3f}, '
                            f'Spec: {results[specificity_weight]["metrics"]["spec"]:.3f}')
    fig.tight_layout()
    return fig, axs


def find_best_bounds(df: pd.DataFrame, specificity_weight: float = 1.75) -> Tuple[float, Dict[str, Any]]:
    """
    Find the best lower and upper bounds for classification based on a given specificity weight.

    :param df: The input DataFrame containing 'gt' and 'aspect_ratio' columns.
    :param specificity_weight: The weight for specificity, defaults to 1.75.
    :return: A dictionary containing the best weighted score, metrics, and bounds.
    """
    lower_bounds = np.linspace(0.6, 1.9, 100)
    upper_bounds = np.linspace(0.6, 1.9, 100)
    best_weighted_score = 0
    best_metrics = {}
    best_bounds = None

    for lower_bound, upper_bound in itertools.product(lower_bounds, upper_bounds):
        classified_df = classify_images(df, lower_bound, upper_bound)
        metrics = get_metrics(classified_df)
        weighted_score = calc_weighted_score(metrics['sens'], metrics['spec'], specificity_weight=specificity_weight)
        if weighted_score > best_weighted_score:
            best_weighted_score = weighted_score
            best_metrics = metrics
            best_bounds = (lower_bound, upper_bound)

    return specificity_weight, {'best_weighted_score': best_weighted_score, 'metrics': best_metrics, 'bounds': best_bounds}


def find_best_bounds_by_weight(df: pd.DataFrame, n: int = 101) -> Dict[float, Dict[str, Any]]:
    """
    Find the best lower and upper bounds for classification for each specificity weight
    in a given DataFrame.

    :param df: A DataFrame containing image information and aspect ratios.
    :param n: The number of specificity weights to consider, defaults to 101.
    :return: A dictionary containing the best bounds and metrics for each specificity weight.
    """
    specificity_weights = np.linspace(1, 2, n)

    with Pool() as p, tqdm(total=len(specificity_weights)) as pbar:
        results = []
        for result in p.starmap(find_best_bounds, ((df, weight) for weight in specificity_weights)):
            results.append(result)
            pbar.update()

    return {specificity_weight: res for specificity_weight, res in results}


def conf_matrix(
        df: pd.DataFrame,
        figsize: Tuple[int] = CONFUSION_MATRIX_FIGSIZE,
) -> Tuple[plt.Figure, plt.Axes]:
    """
    Plot the confusion matrix for the given DataFrame.

    :param df: The input DataFrame containing 'gt' and 'pred' columns.
    :param figsize: The size of the figure, defaults to CONFUSION_MATRIX_FIGSIZE.
    :return: A tuple containing the Figure and Axes objects of the confusion matrix plot.
    """
    return plot_confusion_matrix(
        conf_mat=confusion_matrix(df['gt'], df['pred']), figsize=figsize, cmap=CONFUSION_MATRIX_COLORS['Training set']
    )


def get_metrics(df: pd.DataFrame) -> Dict[str, float]:
    """
    Calculate various classification metrics for the given DataFrame.

    :param df: The input DataFrame containing 'gt' and 'pred' columns.
    :return: A dictionary containing calculated metrics such as accuracy, precision, recall, f1-score, etc.
    """
    gt = df['gt'].astype(int).values
    pred = df['pred'].astype(int).values
    tn, fp, fn, tp = confusion_matrix(gt, pred).ravel()

    return {
        'acc': accuracy_score(gt, pred),
        'precision': precision_score(gt, pred),
        'recall': recall_score(gt, pred),
        'f1': f1_score(gt, pred),
        'sens': tp / (tp + fn) if (tp + fn) != 0 else 0,
        'spec': tn / (tn + fp) if (tn + fp) != 0 else 0,
        'ppv': tp / (tp + fp) if (tp + fp) != 0 else 0,
        'npv': tn / (tn + fn) if (tn + fn) != 0 else 0
    }


def results_by_missing_lung(df: pd.DataFrame) -> pd.DataFrame:
    """
    Aggregate results by the fraction of missing lung.

    :param df: The input DataFrame containing 'fraction', 'pred', and 'name' columns.
    :return: A pivot table DataFrame with the results aggregated by the fraction of missing lung.
    """
    pivot = df.pivot_table(index='fraction', columns='pred', values='name', aggfunc='count')

    index_order = ['nchest', '0-33%', '33-66%', '66-99%', '100%']
    present_categories = [cat for cat in index_order if cat in pivot.index]
    pivot.index = pd.CategoricalIndex(pivot.index, categories=present_categories, ordered=True)
    return pivot.sort_index()


def train_aspect_ratio(
        specificity_weight: float = 1.75,
        shapes_dir: str = ".",
        outdir: str = 'aspect_ratio_training',
        get_results_by_specificity_weight: bool = True,
        n: int = 101
) -> Tuple[float]:
    """
    Tool to train the aspect ratio tool to determine the best lower and upper bounds for classification. Results and
    plots will be saved to the given output directory.

    :param specificity_weight: The weight for specificity, defaults to 1.75.
    :param shapes_dir: The directory containing the shapes files, defaults to "./". These will be
        calculated from the pngs if the file doesn't exist in the directory.
    :param outdir: The output directory, defaults to 'aspect_ratio_training'.
    :param get_results_by_specificity_weight: Whether to get the results by specificity weight for further
        analysis. Defaults to True.
    :param n: The number of specificity weights to consider when plotting by weight. Defaults to 101.
    :return: A tuple containing the best bounds and a dataframe of the metrics for the training set at those bounds.
    """

    def _save_best_bounds(_best_bounds: Tuple[float], _outdir: str):
        with open(os.path.join(_outdir, 'best_bounds_aspect_ratio.txt'), "w") as f:
            f.write(f"Best bounds: {_best_bounds}")
        print("Best bounds: ", _best_bounds)

    def _train_metrics_to_df(_train_metrics: Dict[str, float]) -> pd.DataFrame:
        _temp = {key: [value] for key, value in _train_metrics.items()}
        return pd.DataFrame.from_dict(_temp, orient='index', columns=['value'])

    def _get_results_bw(_df: pd.DataFrame, _n: int, _outdir: str):
        # Get the best bounds and metrics at n specificity weights
        results_bw = find_best_bounds_by_weight(_df, _n)
        results_bw_df = _convert_results_bw_to_df(results_bw)
        results_bw_df.to_csv(os.path.join(_outdir, 'best_bounds_by_weight_aspect_ratio.csv'))

        # Plot histograms at each specificity weight
        results_bw_fig, _ = plot_results_by_weight(_df, results_bw)
        results_bw_fig.savefig(os.path.join(_outdir, 'best_bounds_by_weight_aspect_ratio.png'))

    def _convert_results_bw_to_df(data: Dict[float, Dict[str, Union[float, Dict[str, float]]]]) -> pd.DataFrame:
        formatted_data = {
            key: {**{'best_weighted_score': val['best_weighted_score']}, **val['metrics']}
            for key, val in data.items()
        }

        # Convert the dictionary to a pandas DataFrame
        _df = pd.DataFrame.from_dict(formatted_data, orient='index')
        _df.index.name = "specificity_weight"
        return _df

    print("Training aspect ratio tool...")

    os.mkdirs(outdir, exist_ok=True)

    # Load the labels and aspect ratios
    df = load_labels_and_aspect_ratios('internal_dev', shapes_dir=shapes_dir)

    # Allocate the ground truth
    df = allocate_gt(df)

    # Plot the initial histogram of the aspect ratios in the training set
    initial_fig, _ = plot_histogram(df)
    initial_fig.savefig(os.path.join(outdir, 'train_initial_hist_aspect_ratio.png'))

    # Find the best bounds for the training set using the given specificity weight
    results = find_best_bounds(df, specificity_weight=specificity_weight)
    best_bounds = results[1]['bounds']
    _save_best_bounds(best_bounds, outdir)

    # Plot the best bounds on the training set histogram
    best_bounds_fig, _ = plot_bounds_histogram(df, best_bounds)
    best_bounds_fig.savefig(os.path.join(outdir, 'train_best_bounds_hist_aspect_ratio.png'))

    # Classify the images based on the best bounds
    df = classify_images(df, best_bounds[0], best_bounds[1])
    df.to_csv(os.path.join(outdir, 'train_pred_aspect_ratio.csv'))

    # Save the metrics for the training set using the best bounds
    train_metrics = get_metrics(df)
    _train_metrics_to_df(train_metrics).to_csv(os.path.join(outdir, 'train_metrics_aspect_ratio.csv'))

    # Save the predictions by the amount of lung
    results_by_missing_lung(df).to_csv(os.path.join(outdir, 'train_pred_by_gt_category_aspect_ratio.csv'))

    if get_results_by_specificity_weight:
        _get_results_bw(df, n, outdir)

    print("Done training aspect ratio tool!")

    return best_bounds
