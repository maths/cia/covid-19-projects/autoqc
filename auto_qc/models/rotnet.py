"""
rotnet.py: Rotation Model Definition

This module contains the framework for the rotation model. The rotation model is a convolutional neural network that
predicts the rotation of an image.
"""


from typing import Tuple

from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv2D, Dense, Dropout, Flatten, MaxPooling2D, GlobalAveragePooling2D, ReLU, Add
from tensorflow.keras.optimizers import Adam
from .metrics import angle_error
from .lazytf import K, Sequential
# from .lazytf import Adam, K, Sequential, Conv2D, Dense, Dropout, MaxPooling2D, GlobalAveragePooling2D, ReLU, Add
# from .metrics import angle_error


def RotNet(
    input_shape: Tuple[int, int, int] = (256, 256, 1),
    learning_rate: float = 1e-4,
    loss: str = 'categorical_crossentropy',
    dropout: bool = True,
    nlayers: int = 1,
    nfilters: int = 16,
    nunits: int = 128,
    name: str = None,
) -> Sequential:
    """
    Creates a rotation prediction neural network.

    :param input_shape: Shape of the input images, defaults to (256, 256, 1)
    :param learning_rate: Learning rate for the Adam optimizer, defaults to 1e-4
    :param loss: Loss function to use, defaults to 'categorical_crossentropy'
    :param dropout: Whether to include dropout layers, defaults to True
    :param nlayers: Number of convolutional layers, defaults to 1
    :param nfilters: Number of filters for the convolutional layers, defaults to 16
    :param nunits: Number of units in the dense layer, defaults to 128
    :param name: Name of the model, defaults to None

    :return: A compiled Keras model.
    """

    K.clear_session()


    model = Sequential([
        Conv2D(nfilters, kernel_size=(3, 3), input_shape=input_shape, activation="relu"),
        MaxPooling2D(pool_size=(2, 2)),
    ])

    if nlayers == 2:
        model.add(Conv2D(nfilters + 16, kernel_size=(3, 3), activation="relu",))
        model.add(MaxPooling2D(pool_size=(2, 2)))

    if dropout:
        model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(nunits, activation="relu"))

    if dropout:
        model.add(Dropout(0.25))

    model.add(Dense(360, activation="softmax"))

    model._name = name or "RotNet"
    model.compile(
        optimizer=Adam(learning_rate=learning_rate),
        loss=loss,
        metrics=[angle_error, ])
    model.summary()
    return model
