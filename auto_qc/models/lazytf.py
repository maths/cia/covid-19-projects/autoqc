"""
lazytf.py:  Lazy imports for tensorflow and keras.

This module contains lazy imports for tensorflow and keras.
"""

from ..lazyloader import LazyLoader, LazyAttrib

tf = LazyLoader("tensorflow")
tfa = LazyLoader("tensorflow_addons")
layers = LazyLoader("tensorflow", "keras.layers")
models = LazyLoader("tensorflow", "keras.models")
optimizers = LazyLoader("tensorflow", "keras.optimizers")
K = LazyLoader("tensorflow", "keras.backend")
keras = LazyLoader("tensorflow", "keras")
image = LazyLoader("tensorflow", "keras.preprocessing.image")
utils = LazyLoader("tensorflow", "keras.utils")
callbacks = LazyLoader("tensorflow", "keras.callbacks")
Callback = LazyAttrib(callbacks, "Callback")

Tensor = LazyAttrib(tf, "Tensor")

Add = LazyAttrib(layers, "Add")
Input = LazyAttrib(layers, "Input")
Conv2D = LazyAttrib(layers, "Conv2D")
Dense = LazyAttrib(layers, "Dense")
Dropout = LazyAttrib(layers, "Dropout")
Flatten = LazyAttrib(layers, "Flatten")
MaxPooling2D = LazyAttrib(layers, "MaxPooling2D")
AveragePooling2D = LazyAttrib(layers, "AveragePooling2D")
ReLU = LazyAttrib(layers, "ReLU")
BatchNormalization = LazyAttrib(layers, "BatchNormalization")
RandomRotation = LazyAttrib(layers, "RandomRotation")
RandomZoom = LazyAttrib(layers, "RandomZoom")
RandomContrast = LazyAttrib(layers, "RandomContrast")
RandomHeight = LazyAttrib(layers, "RandomHeight")
RandomWidth = LazyAttrib(layers, "RandomWidth")
RandomBrightness = LazyAttrib(layers, "RandomBrightness")

GlobalAveragePooling2D = LazyAttrib(layers, "GlobalAveragePooling2D")

Sequential = LazyAttrib(models, "Sequential")
Model = LazyAttrib(models, "Model")

Adam = LazyAttrib(optimizers, "Adam")

to_categorical = LazyAttrib(utils, "to_categorical")
array_to_img = LazyAttrib(utils, "array_to_img")
