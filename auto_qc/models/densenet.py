"""
densenet.py: DenseNet121 Model Definition.

This module contains functions for generating DenseNet121 model.
"""

from typing import Tuple, List, Optional
from tensorflow.keras.layers import Lambda, Input, Dense, GlobalAveragePooling2D

from .lazytf import Adam, K, keras, Model, layers, Dropout, tf
from .metrics import f1
from .layers import CropAndResize

LOSS = "binary_crossentropy"
METRICS = [
    keras.metrics.BinaryAccuracy(name="accuracy"),
    keras.metrics.TruePositives(name='tp'),
    keras.metrics.FalsePositives(name='fp'),
    keras.metrics.TrueNegatives(name='tn'),
    keras.metrics.FalseNegatives(name='fn'),
    keras.metrics.Precision(name='precision'),
    keras.metrics.Recall(name='recall'),
    f1,
    keras.metrics.AUC(name='auc'),
    keras.metrics.AUC(name='prc', curve='PR'),  # precision-recall curve
]


def activation_function_checker(activation: str, weights: str, loss: str) -> Tuple[Optional[str], str]:
    if weights and (activation != "softmax" and activation is not None):
        print(f"WARNING: DenseNet121 does not support {activation} activation function when using pretrained weights. "
              "Changed to softmax and loss changed to sparse_categorical_crossentropy.")
        return "softmax", "sparse_categorical_crossentropy"
    return activation, loss


def duplicate_channels(x):
    return tf.repeat(x, repeats=3, axis=-1)


def DenseNet121(
    input_shape: Tuple[int, int, int] = (256, 256, 1),
    learning_rate: float = 1e-3,
    loss: str = None,
    crop: bool = True,
    name: str = None,
    weights: str = None,
    metrics: List = None,
    # activation: str = "sigmoid",
    nunits: int = 1024,
    pooling: str = 'avg',
    nclasses: int = 1,
    dropout: bool = False,
) -> Model:
    """
    Generates a DenseNet121 model.
    """
    loss = loss or LOSS
    metrics = metrics or METRICS
    weights = None if weights == "none" else weights

    # activation, loss = activation_function_checker(activation, weights, loss)

    K.clear_session()

    # Create the input layer (assuming your images are 256x256)
    input_layer = Input(shape=input_shape)
    dimensions = max(input_shape)

    if crop:
        input_layer = CropAndResize(dimensions, dimensions)(input_layer)
    else:
        input_layer = layers.Resizing(dimensions, dimensions)(input_layer)

    # Add the Lambda layer to duplicate the channels
    processed_input = Lambda(duplicate_channels)(input_layer)

    base_model = keras.applications.DenseNet121(
        include_top=False,
        weights=weights,
        input_tensor=processed_input,
        # input_shape=(256, 256, 3),
        pooling=pooling,
        # classes=2,
        # classifier_activation=activation,
    )

    # Add new layers for binary classification
    x = base_model.output

    if dropout:
        x = Dropout(0.25)(x)

    if nclasses == 1:
        out = "sigmoid"
    else:
        out = "softmax"

    x = Dense(nunits, activation='relu')(x)
    output = Dense(1, activation=out)(x)

    model = Model(inputs=base_model.input, outputs=output)

    model._name = name or "DenseNet121"

    model.compile(
        optimizer=Adam(learning_rate=learning_rate),
        loss=loss,
        metrics=metrics)

    # model.summary()

    return model