"""
resnet.py: ResNet Model Definitions.

This module contains functions for generating ResNet models. Each function
returns a compiled Keras model. The models are compiled using the `Adam`
optimizer and the `binary_crossentropy` loss function.
"""

from typing import Tuple, List, Optional
from tensorflow.keras.layers import Lambda, Input, Dense

from .lazytf import Adam, K, keras, Model, Dropout, layers, tf
from .metrics import f1
from .layers import CropAndResize


LOSS = "binary_crossentropy"
METRICS = [
    keras.metrics.BinaryAccuracy(name="accuracy"),
    keras.metrics.TruePositives(name='tp'),
    keras.metrics.FalsePositives(name='fp'),
    keras.metrics.TrueNegatives(name='tn'),
    keras.metrics.FalseNegatives(name='fn'),
    keras.metrics.Precision(name='precision'),
    keras.metrics.Recall(name='recall'),
    f1,
    keras.metrics.AUC(name='auc'),
    keras.metrics.AUC(name='prc', curve='PR'),  # precision-recall curve
]


def activation_function_checker(activation: str, weights: str, loss: str) -> Tuple[Optional[str], str]:
    if weights and (activation != "softmax" and activation is not None):
        print(f"WARNING: DenseNet121 does not support {activation} activation function when using pretrained weights. "
              "Changed to softmax and loss changed to sparse_categorical_crossentropy.")
        return "softmax", "sparse_categorical_crossentropy"
    return activation, loss


def duplicate_channels(x):
    return tf.repeat(x, repeats=3, axis=-1)


def ResNet50(
    input_shape: Tuple[int, int, int] = (256, 256, 1),
    learning_rate: float = 1e-3,
    loss: str = None,
    crop: bool = True,
    name: str = None,
    weights: str = None,
    metrics: List = None,
    # activation: str = "sigmoid",
    nunits: int = 1024,
    pooling: str = 'avg',
    nclasses: int = 1,
    dropout: bool = False,
) -> Model:
    """
    Generates a ResNet50 v1 model.
    """
    loss = loss or LOSS
    metrics = metrics or METRICS
    weights = None if weights == "none" else weights

    # activation, loss = activation_function_checker(activation, weights, loss)

    K.clear_session()

    # Create the input layer (assuming your images are 256x256)
    input_layer = Input(shape=input_shape)
    dimensions = max(input_shape)

    if crop:
        input_layer = CropAndResize(dimensions, dimensions)(input_layer)
    else:
        input_layer = layers.Resizing(dimensions, dimensions)(input_layer)

    # Add the Lambda layer to duplicate the channels
    processed_input = Lambda(duplicate_channels)(input_layer)

    base_model = keras.applications.ResNet50(
        include_top=False,
        weights=weights,
        input_tensor=processed_input,
        input_shape=(dimensions, dimensions, 3),
        pooling=pooling,
        # classes=2,
        # classifier_activation=activation,
    )

    # Add new layers for binary classification
    x = base_model.output

    if dropout:
        x = Dropout(0.25)(x)

    if nclasses == 1:
        out = "sigmoid"
    else:
        out = "softmax"

    x = Dense(nunits, activation='relu')(x)
    output = Dense(1, activation=out)(x)

    model = Model(inputs=base_model.input, outputs=output)

    model._name = name or "ResNet50"

    model.compile(
        optimizer=Adam(learning_rate=learning_rate),
        loss=loss,
        metrics=metrics)

    model.summary()

    return model


def ResNet101(
    input_shape: Tuple[int, int, int] = (256, 256, 1),
    learning_rate: float = 1e-3,
    loss: str = None,
    crop: bool = True,
    name: str = None,
    weights: str = None,
    metrics: List = None,
    # activation: str = "sigmoid",
    nunits: int = 1024,
    pooling: str = 'avg',
    nclasses: int = 1,
    dropout: bool = False,
) -> Model:
    """
    Generates a ResNet101 v1 model.
    """
    loss = loss or LOSS
    metrics = metrics or METRICS
    weights = None if weights == "none" else weights

    # activation, loss = activation_function_checker(activation, weights, loss)

    K.clear_session()

    # Create the input layer (assuming your images are 256x256)
    input_layer = Input(shape=input_shape)
    dimensions = max(input_shape)

    if crop:
        input_layer = CropAndResize(dimensions, dimensions)(input_layer)
    else:
        input_layer = layers.Resizing(dimensions, dimensions)(input_layer)

    # Add the Lambda layer to duplicate the channels
    processed_input = Lambda(duplicate_channels)(input_layer)

    base_model = keras.applications.ResNet101(
        include_top=False,
        weights=weights,
        input_tensor=processed_input,
        input_shape=(dimensions, dimensions, 3),
        pooling=pooling,
        # classes=2,
        # classifier_activation=activation,
    )

    # Add new layers for binary classification
    x = base_model.output

    if dropout:
        x = Dropout(0.25)(x)

    if nclasses == 1:
        out = "sigmoid"
    else:
        out = "softmax"


    x = Dense(nunits, activation='relu')(x)
    output = Dense(1, activation=out)(x)

    model = Model(inputs=base_model.input, outputs=output)

    model._name = name or "ResNet101"

    model.compile(
        optimizer=Adam(learning_rate=learning_rate),
        loss=loss,
        metrics=metrics)

    model.summary()

    return model


def ResNet50V2(
    input_shape: Tuple[int, int, int] = (256, 256, 1),
    learning_rate: float = 1e-3,
    loss: str = None,
    crop: bool = True,
    name: str = None,
    weights: str = None,
    metrics: List = None,
    # activation: str = "sigmoid",
    nunits: int = 1024,
    pooling: str = 'avg',
    nclasses: int = 1,
    dropout: bool = False,
) -> Model:
    """
    Generates a ResNet50 v2 model.
    """
    loss = loss or LOSS
    metrics = metrics or METRICS
    weights = None if weights == "none" else weights

    # activation, loss = activation_function_checker(activation, weights, loss)

    K.clear_session()

    # Create the input layer (assuming your images are 256x256)
    input_layer = Input(shape=input_shape)
    dimensions = max(input_shape)

    if crop:
        input_layer = CropAndResize(dimensions, dimensions)(input_layer)
    else:
        input_layer = layers.Resizing(dimensions, dimensions)(input_layer)

    # Add the Lambda layer to duplicate the channels
    processed_input = Lambda(duplicate_channels)(input_layer)

    base_model = keras.applications.ResNet50V2(
        include_top=False,
        weights=weights,
        input_tensor=processed_input,
        input_shape=(dimensions, dimensions, 3),
        pooling=pooling,
        # classes=2,
        # classifier_activation=activation,
    )

    # Add new layers for binary classification
    x = base_model.output

    if dropout:
        x = Dropout(0.25)(x)

    if nclasses == 1:
        out = "sigmoid"
    else:
        out = "softmax"

    x = Dense(nunits, activation='relu')(x)
    output = Dense(1, activation=out)(x)

    model = Model(inputs=base_model.input, outputs=output)

    model._name = name or "ResNet50V2"

    model.compile(
        optimizer=Adam(learning_rate=learning_rate),
        loss=loss,
        metrics=metrics)

    model.summary()

    return model


def ResNet101V2(
    input_shape: Tuple[int, int, int] = (256, 256, 1),
    learning_rate: float = 1e-3,
    loss: str = None,
    crop: bool = True,
    name: str = None,
    weights: str = None,
    metrics: List = None,
    # activation: str = "sigmoid",
    nunits: int = 1024,
    pooling: str = 'avg',
    nclasses: int = 1,
    dropout: bool = False,
) -> Model:
    """
    Generates a ResNet101 v2 model.
    """
    loss = loss or LOSS
    metrics = metrics or METRICS
    weights = None if weights == "none" else weights

    # activation, loss = activation_function_checker(activation, weights, loss)

    K.clear_session()

    # Create the input layer (assuming your images are 256x256)
    input_layer = Input(shape=input_shape)
    dimensions = max(input_shape)

    if crop:
        input_layer = CropAndResize(dimensions, dimensions)(input_layer)
    else:
        input_layer = layers.Resizing(dimensions, dimensions)(input_layer)

    # Add the Lambda layer to duplicate the channels
    processed_input = Lambda(duplicate_channels)(input_layer)

    base_model = keras.applications.ResNet101(
        include_top=False,
        weights=weights,
        input_tensor=processed_input,
        input_shape=(dimensions, dimensions, 3),
        pooling=pooling,
        # classes=2,
        # classifier_activation=activation,
    )

    # Add new layers for binary classification
    x = base_model.output

    if dropout:
        x = Dropout(0.25)(x)

    if nclasses == 1:
        out = "sigmoid"
    else:
        out = "softmax"

    x = Dense(nunits, activation="relu")(x)
    output = Dense(1, activation=out)(x)

    model = Model(inputs=base_model.input, outputs=output)

    model._name = name or "ResNet101V2"

    model.compile(
        optimizer=Adam(learning_rate=learning_rate),
        loss=loss,
        metrics=metrics)

    return model
