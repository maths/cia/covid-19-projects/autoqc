"""
layers.py: Custom TensorFlow Layers for the AutoQC Package.
"""

from .lazytf import layers, tf, tfa
from skimage import exposure, transform
from functools import partial
import numpy as np


class EqualizeHistLayer(layers.Layer):
    """
    A layer to perform histogram equalization on an image.

    This class inherits from the TensorFlow Keras Layer class.
    It applies the TensorFlow Addons `equalize` function to the input image.
    """

    def __init__(self, **kwargs):
        self.trainable = False
        super().__init__(**kwargs)

    def call(self, x: tf.Tensor) -> tf.Tensor:
        """
        This method is called when the layer is used as a function.

        :param x: The input image.

        :return: The image with histogram equalization applied.
        """
        x_shape = x.shape
        res = tfa.image.equalize(x)
        res.set_shape(x_shape)
        return res


def _py_crop_and_resize(img: np.ndarray, height: int = 256, width: int = 256) -> np.ndarray:
    """
    Crops and resizes an image using scikit-image functions.

    :param img: The input image.
    :param height: The target height for the output image, defaults to 256.
    :param width: The target width for the output image, defaults to 256.

    :return: The cropped and resized image.
    """
    from auto_qc.operations import remove_border
    img_crop = remove_border(img.numpy().squeeze())
    img_res = transform.resize(img_crop, (height, width))
    return img_res[..., None]


def _tf_crop_and_resize(img: tf.Tensor, height: int = 256, width: int = 256) -> tf.Tensor:
    """
    Resizes and crops an image using TensorFlow functions.

    :param img: The input image.
    :param height: The target height for the output image.
    :param width: The target width for the output image.

    :return: The resized and cropped image.
    """
    func = partial(_py_crop_and_resize, height=height, width=width)
    return tf.py_function(func,
                          [img],
                          tf.float32,
                          )


class CropAndResize(layers.Layer):
    """
    A layer to crop and resize an image.

    This class inherits from the TensorFlow Keras Layer class.

    :param height: The target height for the output image.
    :param width: The target width for the output image.
    """

    def __init__(self, height, width, **kwargs):
        self.trainable = False
        self.height = height
        self.width = width
        super().__init__(**kwargs)

    def get_config(self):
        """
        Returns the config of the layer. The same layer can be reinstantiated later
        (without its trained weights) from this configuration.

        :return: The layer configuration.
        """
        config = super().get_config()
        config.update({
            "height": self.height,
            "width": self.width,
        })
        return config

    def call(self, x: tf.Tensor) -> tf.Tensor:
        """
        This method is called when the layer is used as a function.

        :param x: The input image.

        :return: The cropped and resized image.
        """
        x_shape = x.shape
        func = partial(_tf_crop_and_resize, height=self.height, width=self.width)
        res = tf.map_fn(func, x)
        res.set_shape((x.shape[0], self.height, self.width, x_shape[-1]))
        return res

    def compute_output_shape(self, input_shape: tf.TensorShape) -> tf.TensorShape:
        """
        Computes the output shape of the layer.

        :param input_shape: Shape of the input to the layer.

        :return: The output shape of the layer.
        """
        input_shape = tf.TensorShape(input_shape).as_list()
        input_shape[1] = self.height
        input_shape[2] = self.width
        return tf.TensorShape(input_shape)
