"""
comparison_report.py: Functions for generating the comparison report page.

This file contains functions for generating the comparison report page, in which the user can compare the profiling
reports of different values of a column. For example, if the column is 'COVID-19 Diagnosis', the user can compare the
profiling of the data for patients who tested positive for COVID-19 with those who tested negative and identify any
imbalance in the data.
"""

from dash import html, dcc, get_asset_url
import dash_bootstrap_components as dbc
import pandas as pd
import numpy as np
from typing import Optional
import ydata_profiling
from ydata_profiling import ProfileReport

from ..utils import load_data_schema, create_report_settings_dropdown, filter_dataframe_columns, get_title_text, \
    get_ydata_schema


def get_column_search_input(
        df: pd.DataFrame,
        value_maps: dict,
        all_dicom_headers: bool,
        focused_report: bool,
        data_schema_path: str
) -> dbc.Row:
    """
    Create the column search input for the comparison report page.

    :param df: The dataframe to generate the report from.
    :param value_maps: The value maps for the data.
    :param all_dicom_headers: The value of the all_dicom_headers option.
    :param focused_report: The value of the focused_report option.
    :param data_schema_path: The path to the data schema.
    :return: The column search input object for the comparison report page.
    """

    search_columns = [
        col for col in df.columns.tolist() if isinstance(df[col].dtype, pd.CategoricalDtype) and
                                              col not in value_maps["DONT_PLOT_COLS"] and
                                              col in load_data_schema(
            data_schema_path, all_dicom_headers=all_dicom_headers, focused_columns=focused_report
        ).keys()
    ]

    return dbc.Row([
        dbc.Col([
            html.Div([
                dbc.Input(
                    type="gt_col", placeholder="Start typing here...", list="search-column-names", className="me-2",
                    id="split_col-input", debounce=True, autocomplete='off', persistence=True, persistence_type='session',
                ),
                html.Datalist(
                    id="search-column-names",
                    children=[
                        html.Option(value=col) for col in search_columns
                    ]
                ),
            ]),
        ], id="search-column-names-col"),
    ], className="g-0 flex-nowrap")


def create_comparison_report_layout(
    df: pd.DataFrame,
    value_maps: dict,
    all_dicom_headers: bool,
    data_schema_path: str,
    focused_report: Optional[bool] = False
):
    """
    Create the layout for the comparison report page.

    :param df: The dataframe to generate the report from.
    :param value_maps: The value maps for the data.
    :param all_dicom_headers: The value of the all_dicom_headers option.
    :param data_schema_path: The path to the data schema.
    :param focused_report: The value of the focused_report option.
    :return: The layout for the comparison report page.
    """

    return dbc.Container([
        dbc.Row([
            dbc.Col([
                html.P(
                    f"Column to analyse:",
                    className="lead text-end",
                ),
            ], width=2),
            dbc.Col([
                get_column_search_input(df, value_maps, all_dicom_headers, focused_report, data_schema_path),
            ], width=2),
        ], className="gap-1 mt-4 mb-0 flex-nowrap", justify="center", align='baseline'),
        dbc.Row([
            dbc.Col([
                dbc.Stack([
                    html.Div([
                        dbc.Button(
                            'Generate Report', id='generate-comparison-report-button',
                            size="lg", n_clicks=0, disabled=True,
                        )
                    ], id='comp-button-container', className="d-grid col-5 mt-5 ms-auto"),
                    html.Div([
                        create_report_settings_dropdown("comp"),
                    ], className="d-grid col-1 mt-5 me-auto")
                ], direction="horizontal", gap=3),
                html.Iframe(id='comp_report', style={"width": "100%", "height": "80vh"}),
            ], className='g-0 mt-0 gap-0', align='start'),
        ]),
        dbc.Button(
            "Download",
            color="primary",
            id="save-comp-report-button",
        ),
        dcc.Download(id="download-comp-report"),
    ], fluid=True)


def generate_comparison_report(
        df: pd.DataFrame,
        all_dicom_headers: bool,
        column_to_compare: str,
        minimal_report: bool,
        explorative: bool,
        focused_report: bool,
        continuous_var_interactions: bool,
        sample_report: bool,
        sample_size: int,
        seed: int,
        value_maps: dict,
        data_schema_path: str
):
    """Generate profiling report from dataframe and return as HTML string.

    :param df: DataFrame to generate the report from.
    :param column_to_compare: Column to compare.
    :param all_dicom_headers: Whether to include all DICOM headers.
    :param minimal_report: Whether to generate a report using the 'minimal' setting (see YDataProfiling docs).
    :param explorative: Whether to generate a report using the 'explorative' setting (see YDataProfiling docs).
    :param focused_report: Whether to generate a report using a more focused subset of columns if large amounts of data.
    :param continuous_var_interactions: Whether to generate a report with continuous variable interactions calculated (see YDataProfiling docs).
    :param sample_report: Whether to generate a report using a sample of the data.
    :param sample_size: Size of the sample to use.
    :param seed: Seed for the random number generator when sampling.
    :param value_maps: Value maps for the data.
    :param data_schema_path: Path to the data schema.
    :return: str: HTML content of the generated report.
    """

    _df = df.copy()

    _df = filter_dataframe_columns(_df, value_maps['DONT_PLOT_COLS'])
    _df = _df.replace("Missing", np.nan)
    _df = _df.dropna(axis=1, how='all')

    if sample_report:
        _df = _df.sample(n=sample_size, random_state=seed)

    profile_schema = get_ydata_schema(
        _df, data_schema_path, all_dicom_headers=all_dicom_headers, focused_report=focused_report
    )
    _df = _df[[col for col in profile_schema.keys() if col in _df.columns]]
    profile_schema = {key: value for key, value in profile_schema.items() if key in _df.columns}
    include = [col for col in _df.columns if col != column_to_compare]

    from pandas.api.types import is_integer_dtype
    for col in _df.columns:
        if is_integer_dtype(_df[col]):
            _df[col] = _df[col].astype(float)
        elif isinstance(_df[col].dtype, pd.CategoricalDtype):
            _df[col] = _df[col].cat.remove_unused_categories()

    comp_options = ydata_profiling.config.Settings()
    comp_options.title = f"Comparison Report: {get_title_text(column_to_compare)}"
    comp_options.progress_bar = False
    comp_options.report.precision = 6
    # comp_options.html.style.theme = "flatly"
    comp_options.html.style.logo = get_asset_url('autoqc_logo.png')
    comp_options.interactions.continuous = continuous_var_interactions

    if _df[column_to_compare].nunique() == 2:
        value1 = _df[column_to_compare].unique()[0]
        report1 = ProfileReport(
            _df[_df[column_to_compare] == value1][include].dropna(axis=1, how='all'),
            title=f"{value1}",
            minimal=minimal_report,
            explorative=explorative,
            interactions={"continuous": continuous_var_interactions},
            progress_bar=False,
            report={'precision': 6},
            type_schema=profile_schema,
        )
        value2 = _df[column_to_compare].unique()[1]
        report2 = ProfileReport(
            _df[_df[column_to_compare] == value2][include].dropna(axis=1, how='all'),
            title=f"{value2}",
            minimal=minimal_report,
            explorative=explorative,
            interactions={"continuous": continuous_var_interactions},
            progress_bar=False,
            report={'precision': 6},
            type_schema=profile_schema,
        )
        comparison_report = report1.compare(report2, config=comp_options)
    elif _df[column_to_compare].nunique() > 2:
        col_variable_reports = []
        # Show max 4 values
        value_counts = _df[column_to_compare].value_counts(sort=True, ascending=False).head(4)
        for value in value_counts.index:
            sub_df = _df[_df[column_to_compare] == value][include].dropna(axis=1, how='all')
            col_variable_reports.append(
                ProfileReport(
                    sub_df,
                    title=f"{value}",
                    minimal=minimal_report,
                    explorative=explorative,
                    interactions={"continuous": continuous_var_interactions},
                    progress_bar=False,
                    report={'precision': 6},
                    type_schema=profile_schema,
                )
            )
        comparison_report = ProfileReport(col_variable_reports, config=comp_options)
    else:
        return html.Div(
            dbc.Container(
                [
                    html.H1(f"Error - Cannot generate a comparison report", className="text-danger"),
                    html.P(
                        f"The column, {column_to_compare}, has {_df[column_to_compare].nunique()} unique values...",
                        className="lead",
                    ),
                    html.Hr(className="my-2"),
                    html.P(
                        "Please choose another column or just use the Profiling Report tool.",
                        className="lead",
                    ),
                ],
                fluid=True,
                className="py-3",
            ),
            className="p-3 bg-light rounded-3",
        )

    report_html = comparison_report.to_html()
    return report_html




