"""
dicom_comparison.py: Contains the functions for generating the DICOM-AutoQC comparison page.

The DICOM-AutoQC comparison page allows the user to compare the AutoQC predictions to the DICOM metadata. The page
contains three tabs: 'Grayscale Inversion', 'Projection', and 'Anatomical Area'. Each tab contains a table and a bar
chart comparing the AutoQC predictions to the DICOM metadata.
"""

import dash_bootstrap_components as dbc
import pandas as pd
from dash import html
import plotly.graph_objects as go
from typing import Optional

from ..utils import create_plot_button_group


PROJECTION_MAP = {
    "fixed_case": {
        "AP": "AP",
        "PA": "PA",
        "ap": "AP",
        "pa": "PA",
        "LL": "Lateral",
        "RL": "Lateral",
        "RLD": "Lateral Decubitus",
        "LLD": "Lateral Decubitus",
        "RLO": "Oblique",
        "LLO": "Oblique",
        "RPO": "Oblique",
        "LPO": "Oblique",
    },
    "variable_case": {
        "lateral": "Lateral",
        "lat": "Lateral",
        "anteroposterior": "AP",
        "posteroanterior": "PA",
        "antero-posterior": "AP",
        "postero-anterior": "PA",
        "antero_posterior": "AP",
        "postero_anterior": "PA",
        "antero posterior": "AP",
        "postero anterior": "PA",
        "lateral decubitus": "Lateral Decubitus",
        "oblique": "Oblique",
        "anterior oblique": "Oblique",
        "posterior oblique": "Oblique",
    }
}

ANATOMY_MAP = {
    "chest": "chest",
    "breast": "not_chest",
    "abdomen": "not_chest",
    "abdo": "not_chest",
    "lung": "chest",
    "hand": "not_chest",
    "foot": "not_chest",
    "leg": "not_chest",
    "neck": "not_chest",
    "head": "not_chest",
    "cranium": "not_chest",
    "pelvis": "not_chest",
    "thorax": "chest",
    "thoracic": "chest",
    "ribs": "chest",
    "rib": "chest",
    "shoulder": "not_chest",
    "skull": "not_chest",
    "lspine": "not_chest",
    "cspine": "not_chest",
    "tspine": "not_chest",
    "lumbar": "not_chest",
    "cervical": "not_chest",
    "thoracolumbar": "not_chest",
    "spine": "not_chest",
    "knee": "not_chest",
    "port chest": "chest",
    "portable chest": "chest",
    "mobile chest": "chest",
}

proj_dicom_method_button_group = create_plot_button_group("proj_plot-radios", "DICOM Headers:", [
    {"label": "Combined", "value": "combined-headers"},
    {"label": "View Headers", "value": "view-headers"},
    {"label": "Series Description", "value": "series-description"},
    {"label": "Study Description", "value": "study-description"},
], 'combined-headers')

anatomy_dicom_method_button_group = create_plot_button_group("anatomy_plot-radios", "DICOM Headers:", [
    {"label": "Combined", "value": "combined-headers"},
    {"label": "Body Part / Anatomy Headers", "value": "anatomy-headers"},
    {"label": "Series Description", "value": "series-description"},
    {"label": "Study Description", "value": "study-description"},
], 'combined-headers')

inv_dicom_method_button_group = create_plot_button_group("inv_plot-radios", "DICOM Headers:", [
    {"label": "Photometric Interpretation", "value": "PhotometricInterpretation"},
], 'PhotometricInterpretation')


def generate_dicom_table(df: pd.DataFrame, col: str) -> dbc.Table:
    """
    Generates a table comparing the AutoQC inversion predictions to the DICOM metadata.

    :param df: The dataframe containing the 'AutoQC' and 'PhotometricInterpretation' columns.
    :param col: The column to compare the AutoQC predictions to.
    :return: A table comparing the AutoQC inversion predictions to the DICOM metadata.
    """
    counts = df[[col, 'AutoQC']].value_counts()
    proportions = df[[col, 'AutoQC']].value_counts(normalize=True)

    table = pd.DataFrame({'Counts': counts, 'Proportions': proportions}).rename_axis(['DICOM', 'AutoQC']).sort_index(
        axis=0)

    table.Counts = table.Counts.apply(lambda x: f'{x:,}')
    table.Proportions = table.Proportions.apply(lambda x: f'{x:.2%}')

    return dbc.Table.from_dataframe(
        table,
        striped=True, bordered=True, hover=True, index=True, id='dicom-table'
    )


def generate_dicom_plot(df: pd.DataFrame, col: str) -> go.Figure:
    """
    Generates a bar chart comparing the AutoQC inversion predictions to the DICOM metadata.

    :param df: The dataframe containing the 'AutoQC' and 'PhotometricInterpretation' columns.
    :param col: The column to compare the AutoQC predictions to.
    :return: The bar chart comparing the AutoQC inversion predictions to the DICOM metadata.
    """
    count1 = df[col].value_counts()
    count2 = df['AutoQC'].value_counts()

    fig = go.Figure(data=[
        go.Bar(name='DICOM Metadata', x=count1.index, y=count1.values, opacity=0.6),
        go.Bar(name='AutoQC Prediction', x=count2.index, y=count2.values, opacity=0.6)
    ])
    fig.update_layout(barmode='group',
                      yaxis_title='Frequency')
    return fig


def map_and_deduplicate(x: Optional[str], value_map: dict) -> Optional[str]:
    """
    Maps the values in a string comma-separated sequence to a new value, and removes duplicates.

    :param x:
    :param value_map:
    :return:
    """
    # Split the string into individual values, map them, and remove duplicates
    if pd.isna(x):
        return x
    unique_values = set(x.split(', '))  # Split by comma and space, then convert to set to remove duplicates
    mapped_values = [value_map.get(value, value) for value in unique_values]  # Map values, default to original value if not found in map
    return ', '.join(sorted(mapped_values))


def extrapolate_inversion_predictions_back_to_dicom(df: pd.DataFrame) -> pd.DataFrame:
    """
    As AutoQC initially uses the DICOM metadata to correct inversion, we need to extrapolate the inversion predictions
    back to the original image.

    For example, if the DICOM metadata says the image is inverted, it should be flipped in preprocessing. If the
    AutoQC prediction is then not inverted, this means that the DICOM was correct; however, if it predicts inverted,
    then it suggests that the DICOM was incorrect.

    :param df: The dataframe with only the 'inv_pred' and 'PhotometricInterpretation' columns.
    :return: The dataframe with an additional 'AutoQC' column with the extrapolated inversion predictions.
    """
    df.loc[(df.PhotometricInterpretation == 'inverted') & (df.inv_pred == 'not_inverted'), 'AutoQC'] = "inverted"
    df.loc[(df.PhotometricInterpretation == 'not_inverted') & (df.inv_pred == 'inverted'), 'AutoQC'] = "inverted"
    df.loc[
        (df.PhotometricInterpretation == 'not_inverted') & (df.inv_pred == 'not_inverted'), 'AutoQC'] = "not_inverted"
    df.loc[(df.PhotometricInterpretation == 'inverted') & (df.inv_pred == 'inverted'), 'AutoQC'] = "not_inverted"
    df.loc[(df.PhotometricInterpretation == 'Missing') & (df.inv_pred == 'inverted'), 'AutoQC'] = "inverted"
    df.loc[(df.PhotometricInterpretation == 'Missing') & (df.inv_pred == 'not_inverted'), 'AutoQC'] = "not_inverted"
    df.AutoQC = pd.Categorical(df.AutoQC, categories=["inverted", "not_inverted", "Missing"])
    return df


def get_inversion_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Extracts the inversion data from the dataframe and extrapolates the inversion predictions back to a prediction for
    the original image.

    :param df: Dataframe containing the 'inv_pred' and 'PhotometricInterpretation' columns
    :return: Dataframe containing the 'AutoQC' column with the extrapolated inversion predictions
    """
    inv = df[['inv_pred', 'PhotometricInterpretation']].copy()
    inv['PhotometricInterpretation'] = inv['PhotometricInterpretation'].replace({
        'MONOCHROME1': 'inverted',
        'MONOCHROME2': 'not_inverted'
    })
    inv['PhotometricInterpretation'] = pd.Categorical(inv['PhotometricInterpretation'],
                                                      categories=['inverted', 'not_inverted', 'Missing'])
    inv = extrapolate_inversion_predictions_back_to_dicom(inv)
    return inv


def get_projection_map() -> dict:
    """
    Generates a map of projection names to a standardised format with multiple possible representations.

    :return: dictionary mapping projection values to a standardised format
    """
    fixed_case = PROJECTION_MAP['fixed_case']
    variable_case = PROJECTION_MAP['variable_case']
    variable_case_extended = {}
    for key, value in variable_case.items():
        transformations = {
            key.lower(),
            key.upper(),
            key.title(),
            key.replace(' ', '_'),
            key.replace(' ', '-')
        }

        for transformed_key in transformations:
            variable_case_extended[transformed_key] = value
    projection_map = {**fixed_case, **variable_case_extended}
    return projection_map


def get_overall_dicom_projection(df: pd.DataFrame) -> pd.DataFrame:
    """
    Generates an 'Overall Projection' column by combining the 'ViewPosition', 'ViewCodeSequence-CodeMeaning',
    'SeriesDescription', and 'StudyDescription' headers. It also generates a 'ViewHeaders' column by combining the
    'ViewPosition' and 'ViewCodeSequence-CodeMeaning' headers.

    :param df: The dataframe containing the DICOM metadata.
    :return: The dataframe with 'OverallProjection' and 'ViewHeaders' columns.
    """
    if "ViewPosition" in df.columns:
        df["OverallProjection"] = df["ViewPosition"].copy()
        df["ViewHeaders"] = df["ViewPosition"].copy()
    if "ViewCodeSequence-CodeMeaning" in df.columns:
        df.loc[df["OverallProjection"].isna(), 'OverallProjection'] = df["ViewCodeSequence-CodeMeaning"]
        if "ViewHeaders" in df.columns:
            df.loc[df["ViewHeaders"].isna(), 'ViewHeaders'] = df["ViewCodeSequence-CodeMeaning"]
        else:
            df["ViewHeaders"] = df["ViewCodeSequence-CodeMeaning"]
    if "SeriesDescription-Views" in df.columns:
        df.loc[df["OverallProjection"].isna(), 'OverallProjection'] = df["SeriesDescription-Views"]
    if "StudyDescription-Views" in df.columns:
        df.loc[df["OverallProjection"].isna(), 'OverallProjection'] = df["StudyDescription-Views"]
    return df


def get_overall_projection_prediction(df: pd.DataFrame) -> pd.DataFrame:
    """
    Generates an 'AutoQC' column by combining the 'lat_pred', 'proj_ocr_pred', and 'proj_dl_pred' columns.

    :param df: The dataframe containing the AutoQC predictions.
    :return: The dataframe with an 'AutoQC' column.
    """
    df["AutoQC"] = pd.NA
    if "lat_pred" in df.columns:
        df.loc[df["lat_pred"] == "lateral", 'AutoQC'] = "lateral"

    if "proj_ocr_pred" in df.columns:
        df.loc[df["AutoQC"].isna(), 'AutoQC'] = df['proj_ocr_pred']
        df.loc[df["AutoQC"].isna(), 'AutoQC'] = df['proj_dl_pred']
    elif "proj_dl_pred" in df.columns:
        df.loc[:, 'AutoQC'] = df['proj_dl_pred']
    df.AutoQC = df.AutoQC.map({"ap": "AP", "pa": "PA", "lateral": "Lateral", "Missing": "Missing"})
    return df


def process_projection_columns(df: pd.DataFrame) -> pd.DataFrame:
    """
    Processes the projection columns by mapping the values to a standardised format and combining the values into a
    single overall column for the DICOM headers and the AutoQC predictions.

    :param df: The dataframe containing the projection columns.
    :return: The dataframe with the projection columns processed.
    """
    proj_map = get_projection_map()
    description_pattern = r'\b(' + '|'.join(proj_map.keys()) + r')\b'

    if 'ViewPosition' in df.columns:
        df.ViewPosition = df.ViewPosition.map(proj_map)
    if "ViewCodeSequence-CodeMeaning" in df.columns:
        df['ViewCodeSequence-CodeMeaning'] = df['ViewCodeSequence-CodeMeaning'].map(proj_map)
    if 'SeriesDescription' in df.columns:
        df['SeriesDescription-Views'] = df['SeriesDescription'].str.extractall(
            description_pattern
        ).groupby(level=0).apply(lambda x: ', '.join(x[0])).reindex(df.index).apply(map_and_deduplicate, args=(proj_map,))
        df.loc[df['SeriesDescription-Views'].str.contains(", ", na=False), "SeriesDescription-Views"] = "Multiple"
        # df.loc[
        #     df['SeriesDescription-Views'] != "Multiple", 'SeriesDescription-Views'
        # ] = df['SeriesDescription-Views'].map(proj_map)
    if 'StudyDescription' in df.columns:
        df['StudyDescription-Views'] = df['StudyDescription'].str.extractall(
            description_pattern
        ).groupby(level=0).apply(lambda x: ', '.join(x[0])).reindex(df.index).apply(map_and_deduplicate, args=(proj_map,))
        df.loc[df['StudyDescription-Views'].str.contains(", ", na=False), "StudyDescription-Views"] = "Multiple"
        # df.loc[
        #     df['StudyDescription-Views'] != "Multiple", 'StudyDescription-Views'
        # ] = df['StudyDescription-Views'].map(proj_map)

    return get_overall_dicom_projection(get_overall_projection_prediction(df))


def get_projection_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Extracts the projection data from the dataframe and processes the projection columns.

    :param df: The dataframe containing the projection columns.
    :return: The dataframe containing the processed projection columns.
    """
    cols = [
        'lat_pred', 'proj_dl_pred', 'proj_ocr_pred', 'ViewPosition',
        'SeriesDescription', 'StudyDescription', 'ViewCodeSequence-CodeMeaning'
    ]
    cols = [col for col in cols if col in df.columns]
    proj = df[cols].copy()
    for col in cols:
        proj[col] = proj[col].astype(object)
    proj = proj.replace("Missing", pd.NA)
    proj = process_projection_columns(proj)
    proj = proj.fillna("Missing")
    for col in cols:
        proj[col] = proj[col].astype('category')
    return proj


def get_anatomy_map() -> dict:
    """
    Generates a map of anatomy names to a standardised format with multiple possible representations.

    :return: Dictionary mapping anatomy values to a standardised format
    """
    variable_case_anatomy = {}
    for key, value in ANATOMY_MAP.items():
        transformations = {
            key.lower(),
            key.upper(),
            key.title(),
            key.replace(' ', '_'),
            key.replace(' ', '-')
        }

        for transformed_key in transformations:
            variable_case_anatomy[transformed_key] = value
    return variable_case_anatomy


def get_overall_dicom_anatomy(df: pd.DataFrame) -> pd.DataFrame:
    """
    Generates an 'Overall Anatomy' column by combining the 'BodyPartExamined', 'AnatomicRegionSequence-CodeMeaning',
    'SeriesDescription', and 'StudyDescription' headers. It also generates an 'AnatomyHeaders' column by combining the
    'BodyPartExamined' and 'AnatomicRegionSequence-CodeMeaning' headers.

    :param df: The dataframe containing the DICOM metadata.
    :return: The dataframe with 'OverallAnatomy' and 'AnatomyHeaders' columns.
    """
    if "BodyPartExamined" in df.columns:
        df["OverallAnatomy"] = df["BodyPartExamined"].copy()
        df["AnatomyHeaders"] = df["BodyPartExamined"].copy()
    if "AnatomicRegionSequence-CodeMeaning" in df.columns:
        df.loc[df["OverallAnatomy"].isna(), 'OverallAnatomy'] = df["AnatomicRegionSequence-CodeMeaning"]
        if "AnatomyHeaders" in df.columns:
            df.loc[df["AnatomyHeaders"].isna(), 'AnatomyHeaders'] = df["AnatomicRegionSequence-CodeMeaning"]
        else:
            df["AnatomyHeaders"] = df["AnatomicRegionSequence-CodeMeaning"]
    if "SeriesDescription-Anatomy" in df.columns:
        df.loc[df["OverallAnatomy"].isna(), 'OverallAnatomy'] = df["SeriesDescription-Anatomy"]
    if "StudyDescription-Anatomy" in df.columns:
        df.loc[df["OverallAnatomy"].isna(), 'OverallAnatomy'] = df["StudyDescription-Anatomy"]
    return df


def process_anatomy_columns(df: pd.DataFrame) -> pd.DataFrame:
    """
    Processes the anatomy columns by mapping the values to a standardised format and combining the values into a
    single overall column for the DICOM headers and the AutoQC predictions.

    :param df: The dataframe containing the anatomy columns.
    :return: The dataframe with the anatomy columns processed.
    """
    anatomy_map = get_anatomy_map()
    description_pattern = r'\b(' + '|'.join(anatomy_map.keys()) + r')\b'

    if 'BodyPartExamined' in df.columns:
        df.BodyPartExamined = df.BodyPartExamined.map(anatomy_map)
    if "AnatomicRegionSequence-CodeMeaning" in df.columns:
        df['AnatomicRegionSequence-CodeMeaning'] = df['AnatomicRegionSequence-CodeMeaning'].map(anatomy_map)
    if 'SeriesDescription' in df.columns:
        df['SeriesDescription-Anatomy'] = df['SeriesDescription'].str.extractall(
            description_pattern
        ).groupby(level=0).apply(lambda x: ', '.join(x[0])).reindex(df.index).apply(map_and_deduplicate, args=(anatomy_map,))
        df.loc[df['SeriesDescription-Anatomy'].str.contains(", ", na=False), "SeriesDescription-Anatomy"] = "chest+other"
        # df.loc[
        #     df['SeriesDescription-Anatomy'] != "Chest+Other", 'SeriesDescription-Anatomy'
        # ] = df['SeriesDescription-Anatomy'].map(anatomy_map)
    if 'StudyDescription' in df.columns:
        df['StudyDescription-Anatomy'] = df['StudyDescription'].str.extractall(
            description_pattern
        ).groupby(level=0).apply(lambda x: ', '.join(x[0])).reindex(df.index).apply(map_and_deduplicate, args=(anatomy_map,))
        df.loc[df['StudyDescription-Anatomy'].str.contains(", ", na=False), "StudyDescription-Anatomy"] = "chest+other"
        # df.loc[
        #     df['StudyDescription-Anatomy'] != "Chest+Other", 'StudyDescription-Anatomy'
        # ] = df['StudyDescription-Anatomy'].map(anatomy_map)
    if "ar_pred" in df.columns:
        df.loc[:, 'AutoQC'] = df['ar_pred'].replace({"keep": "Chest", "exclude": "Other", "review": "Other", "remove": "Other"})

    return get_overall_dicom_anatomy(df)


def get_anatomy_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Extracts the anatomy data from the dataframe and processes the anatomy columns.

    :param df: The dataframe containing the anatomy columns.
    :return: The dataframe containing the processed anatomy columns.
    """
    cols = [
        'ar_pred', "AnatomicRegionSequence-CodeMeaning", "BodyPartExamined", "SeriesDescription", "StudyDescription"
    ]
    cols = [col for col in cols if col in df.columns]
    anat = df[cols].copy()
    for col in cols:
        anat[col] = anat[col].astype(object)
    anat = anat.replace("Missing", pd.NA)
    anat = process_anatomy_columns(anat)
    anat = anat.fillna("Missing")
    for col in cols:
        anat[col] = anat[col].astype('category')
    return anat


def create_qc_dicom_comp_layout():
    """
    Creates the layout for the DICOM comparison page.

    :return: The layout for the DICOM comparison page.
    """
    return dbc.Container([
        dbc.Row([
            dbc.Col([
                dbc.Card([
                    dbc.CardHeader(dbc.Stack(
                        [
                            html.H5("Comparison of AutoQC Output and DICOM Metadata",
                                    className='me-auto mb-0 g-0 mt-auto'),
                            dbc.Tabs(
                                [
                                    dbc.Tab(label="Grayscale Inversion", tab_id="inv-tab", id="inv-tab"),
                                    dbc.Tab(label="Projection", tab_id="projection-tab", id="projection-tab"),
                                    dbc.Tab(label="Anatomical Area", tab_id="anatomy-tab", id="anatomy-tab"),
                                ], id="dicom-comp-card-tabs", active_tab="inv-tab", persistence=True,
                                persistence_type="session",
                            ),
                        ], direction="horizontal", className="align-items-center")
                    ),
                    dbc.CardBody(dbc.Container(id="dicom-comp-card-content", className="h-100"),
                                 className="h-100", style={"overflow-y": "auto"}),
                ], className='h-100 g-0'),
            ])
        ],
            # style={"height": "90vh"},
            className='mt-3 mb-3 g-0'
        )
    ], fluid=True)
