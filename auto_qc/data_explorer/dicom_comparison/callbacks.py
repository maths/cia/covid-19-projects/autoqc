"""
callbacks.py: Callbacks for the DICOM-AutoQC comparison card.

This script contains the callbacks for the DICOM-AutoQC comparison card.
"""

import dash
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
from dash import dcc
import pandas as pd

from ..utils import callback_if_pathname_matches
from .dicom_comparison import (get_inversion_data, generate_dicom_table, generate_dicom_plot, get_projection_data,
                               get_anatomy_data, proj_dicom_method_button_group, anatomy_dicom_method_button_group,
                               inv_dicom_method_button_group)


proj_method_map = {
    "series-description": "SeriesDescription-Views",
    "study-description": "StudyDescription-Views",
    "view-headers": "ViewHeaders",
    "combined-headers": "OverallProjection"
}

anat_method_map = {
    "series-description": "SeriesDescription-Anatomy",
    "study-description": "StudyDescription-Anatomy",
    "anatomy-headers": "AnatomyHeaders",
    "combined-headers": "OverallAnatomy",
}

anatomy_presentation_map = {
    "chest+other": "Chest and Other", "chest": "Chest", "not_chest": "Other", "Missing": "Missing"
}

inv_presentation_map = {"inverted": "Inverted", "not_inverted": "Not Inverted", "Missing": "Missing"}


@dash.callback(
    Output('dicom-comp-card-content', 'children'),
    [Input("dicom-comp-card-tabs", "active_tab"),
     Input('url', 'pathname')],
    [State("dataframe", 'data'),
     State("dtypes", 'data')]
)
@callback_if_pathname_matches('/qc_dicom_comp')
def generate_dcm_comparison_card(active_tab, url, json_data, dtypes):
    """
    Generates the content of the DICOM-AutoQC comparison card.

    :param active_tab: The active tab of the card
    :param url: The current url
    :param json_data: The json data of the dataframe
    :param dtypes: The dtypes of the dataframe
    :return: The content of the card
    """

    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)

    if active_tab == "inv-tab":
        inv = get_inversion_data(df)
        inv = inv.applymap(lambda x: inv_presentation_map[x] if x in inv_presentation_map else x)
        return dbc.Container([
            inv_dicom_method_button_group,
            dbc.Row([
                dbc.Col([
                    dbc.Container([
                        generate_dicom_table(inv, "PhotometricInterpretation"),
                    ], id='inv-table')
                ]),
                dbc.Col([
                    dcc.Graph(id='inv-plot', figure=generate_dicom_plot(inv, "PhotometricInterpretation"))],
                    className='g-0', style={'height': '90%'}),
            ], align='center')
        ], className='h-100 g-0')

    elif active_tab == "projection-tab":
        dcm_method = "combined-headers"  # all dicom headers
        proj = get_projection_data(df)
        return dbc.Container([
            proj_dicom_method_button_group,
            dbc.Row([
                dbc.Col([
                    dbc.Container([
                        generate_dicom_table(proj, proj_method_map[dcm_method]),
                    ], id='proj-table')
                ]),
                dbc.Col([
                    dcc.Graph(id='proj-plot', figure=generate_dicom_plot(proj, proj_method_map[dcm_method]))],
                    className='g-0', style={'height': '90%'}),
            ], align='center')
        ], className='h-100 g-0')

    elif active_tab == "anatomy-tab":
        anatomy_method = "combined-headers"
        anat = get_anatomy_data(df)
        anat = anat.applymap(lambda x: anatomy_presentation_map[x] if x in anatomy_presentation_map else x)
        return dbc.Container([
            anatomy_dicom_method_button_group,
            dbc.Row([
                dbc.Col([
                    dbc.Container([
                        generate_dicom_table(anat, anat_method_map[anatomy_method]),
                    ], id='anatomy-table')
                ]),
                dbc.Col([
                    dcc.Graph(id='anatomy-plot', figure=generate_dicom_plot(anat, anat_method_map[anatomy_method]))],
                    className='g-0', style={'height': '90%'}),
            ], align='center')
        ], className='h-100 g-0')

    else:
        return None


@dash.callback(
    [Output('proj-plot', 'figure'),
     Output('proj-table', 'children')],
    [Input('proj_plot-radios', 'value'),
     Input('url', 'pathname')],
    [State("dataframe", 'data'),
     State("dtypes", 'data')]
)
@callback_if_pathname_matches('/qc_dicom_comp')
def update_proj_comparison(dcm_method, url, json_data, dtypes):
    """
    Updates the projection plot and table based on the selected DICOM method.

    :param dcm_method: The method coding for the DICOM column/s to use
    :param url: The current url
    :param json_data: The json data of the dataframe
    :param dtypes: The dtypes of the dataframe
    :return: The updated projection plot and table
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)
    proj = get_projection_data(df)
    fig_object = generate_dicom_plot(proj, proj_method_map[dcm_method])
    table_object = generate_dicom_table(proj, proj_method_map[dcm_method])
    return fig_object, table_object


@dash.callback(
    [Output('anatomy-plot', 'figure'),
     Output('anatomy-table', 'children')],
    [Input('anatomy_plot-radios', 'value'),
     Input('url', 'pathname')],
    [State("dataframe", 'data'),
     State("dtypes", 'data')]
)
@callback_if_pathname_matches('/qc_dicom_comp')
def update_proj_comparison(dcm_method, url, json_data, dtypes):
    """
    Updates the anatomy/body part plot and table based on the selected DICOM method.

    :param dcm_method: The method coding for the DICOM column/s to use
    :param url: The current url
    :param json_data: The json data of the dataframe
    :param dtypes: The dtypes of the dataframe
    :return: The updated plot and table
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)
    anat = get_anatomy_data(df)
    anat = anat.applymap(lambda x: anatomy_presentation_map[x] if x in anatomy_presentation_map else x)
    fig_object = generate_dicom_plot(anat, anat_method_map[dcm_method])
    table_object = generate_dicom_table(anat, anat_method_map[dcm_method])
    return fig_object, table_object
