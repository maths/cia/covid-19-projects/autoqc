"""
profile_report.py: Generate the profiling report from a dataframe.

This module contains the functions to generate the profiling report from a dataframe. The report is generated using the
YDataProfiling library. The report is generated using the settings specified in the data schema. The report offers
interactive visualisations of the data, as well as a summary of the data, missing values and its distributions.
"""

from dash import html, dcc, get_asset_url
import dash_bootstrap_components as dbc
import pandas as pd
import numpy as np
from ydata_profiling import ProfileReport

from ..utils import create_report_settings_dropdown, filter_dataframe_columns, get_ydata_schema


def generate_profile_report(
        df: pd.DataFrame,
        all_dicom_headers: bool,
        minimal_report: bool,
        explorative: bool,
        focused_report: bool,
        continuous_var_interactions: bool,
        sample_report: bool,
        sample_size: int,
        seed: int,
        value_maps: dict,
        data_schema_path: str,
):
    """Generate profiling report from dataframe and return as HTML string.

    :param df: DataFrame to generate the report from.
    :param all_dicom_headers: Whether to include all DICOM headers.
    :param minimal_report: Whether to generate a report using the 'minimal' setting (see YDataProfiling docs).
    :param explorative: Whether to generate a report using the 'explorative' setting (see YDataProfiling docs).
    :param focused_report: Whether to generate a report using a more focused subset of columns if large amounts of data.
    :param continuous_var_interactions: Whether to generate a report with continuous variable interactions calculated (see YDataProfiling docs).
    :param sample_report: Whether to generate a report using a sample of the data.
    :param sample_size: Size of the sample to use.
    :param seed: Seed for the random number generator when sampling.
    :param value_maps: Value maps to use for the report.
    :param data_schema_path: Path to the data schema.
    :return: str: HTML content of the generated report.
    """
    report_df = df.copy()

    report_df = filter_dataframe_columns(report_df, value_maps['DONT_PLOT_COLS'])

    if sample_report:
        report_df = report_df.sample(n=sample_size, random_state=seed)

    profile_schema = get_ydata_schema(
        report_df, data_schema_path, all_dicom_headers=all_dicom_headers, focused_report=focused_report
    )
    report_df = report_df.replace("Missing", np.nan)
    report_df = report_df.dropna(axis=1, how='all')
    report_df = report_df[[col for col in list(profile_schema.keys()) if col in report_df.columns]]
    profile_schema = {key: value for key, value in profile_schema.items() if key in report_df.columns}

    from pandas.api.types import is_integer_dtype
    for col in report_df.columns:
        if is_integer_dtype(report_df[col]):
            report_df[col] = report_df[col].astype(float)
        elif isinstance(report_df[col].dtype, pd.CategoricalDtype):
            report_df[col] = report_df[col].cat.remove_unused_categories()

    report = ProfileReport(
        report_df,
        title='Profiling Report',
        minimal=minimal_report,
        explorative=explorative,
        progress_bar=False,
        interactions={"continuous": continuous_var_interactions},
        html={"style": {
            "theme": 'flatly',  # flatly, simplex, cosmo, united
            "logo": get_asset_url('autoqc_logo.png'),
            # "primary_color": "#337ab7",
            "full_width": True
        }},
        report={'precision': 6},
        # Types allowed in the schema: Boolean, Numeric, Date (and Datetime), Categorical, Time-series, URL, Path,
        # File, Image
        type_schema=profile_schema,
    )

    report_html = report.to_html()
    return report_html


def create_profile_report_layout() -> dbc.Container:
    """
    Create the layout for the profile report page.

    :return: Layout for the profile report page.
    """
    return dbc.Container([
        dbc.Row([
            dbc.Col([
                dbc.Stack([
                    html.Div([
                        dbc.Button(
                            'Generate Report', id='generate-report-button', size="lg", n_clicks=0
                        )], id='profile-button-container', className="d-grid col-5 mt-5 ms-auto"),
                    html.Div([
                        create_report_settings_dropdown("norm"),
                    ], className="d-grid col-1 mt-5 me-auto")
                ], direction="horizontal", gap=3),
                html.Iframe(id='profile-report', style={"width": "100%", "height": "85vh"}),
                dbc.Button(
                    "Download",
                    color="primary",
                    id="save-report-button",
                ),
                dcc.Download(id="download-report"),
            ], className='g-0'),
        ], className="mb-3 g-0"),
    ], fluid=True, className='g-0')
