"""
utils.py: Utility functions for the data explorer Dash app.
"""

import logging
import pandas as pd
from pathlib import Path
import yaml
import ast
from typing import Dict, List, Union, Optional, Any
import numpy as np
from dash.exceptions import PreventUpdate
from dash import callback_context, html
import dash_bootstrap_components as dbc
from pandas.api.types import is_numeric_dtype, is_datetime64_any_dtype, is_categorical_dtype


def tidy_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Converts column names to lowercase and replaces spaces with underscores.

    :param df: The input DataFrame.
    :return: The tidied DataFrame.
    """
    _df = df.copy()
    # _df.columns = [col.lower().replace(' ', '_') for col in _df.columns]
    for col in ['path', 'patient_id']:
        if col in _df.columns:
            _df = _df.drop(col, axis=1)
    return _df


def try_to_make_name_column(df: pd.DataFrame) -> Optional[pd.DataFrame]:
    """
    Attempts to create a 'name' column from a 'path' column if it exists, or a 'filename' column if it exists.

    :param df: The input DataFrame.
    :return: The DataFrame with a 'name' column, or None if no 'path' or 'filename' column exists.
    """
    _df = df.copy()
    for column in _df.columns:
        if 'path' in column:
            _df['name'] = _df[column].apply(lambda x: Path(x).stem).str.replace(".png", "", regex=False).str.replace(
                ".dcm", "", regex=False)
            print(f"WARNING: 'name' column not found. Created 'name' column from '{column}' column instead.")
            return _df
    for column in _df.columns:
        if column in ['filename', 'file_name', 'file', 'png', 'png_file', 'png_filename', 'png_file_name',
                      'png_path', 'png_file_path', 'dcm', 'dicom', 'dicom_file', 'dicom_filename', 'dicom_file_name',
                      'dicom_path', 'dicom_file_path']:
            _df['name'] = _df[column]
            print(f"WARNING: 'name' column not found. Created 'name' column from '{column}' column instead.")
            return _df
    return None


def load_data(
        file_paths: List[Union[str, Path]],
        join_on: str = "name",
        how: str = 'left'
) -> pd.DataFrame:
    """
    Loads data from a list of CSV files and merges them on a specified column.

    :param file_paths: The list of file paths.
    :param join_on: The column to merge on.
    :param how: The type of merge to perform.
    :return: The merged DataFrame.
    """
    dfs = []
    if len(file_paths) == 1:
        df_merged = pd.read_csv(file_paths[0], dtype=str)
        df_merged = tidy_df(df_merged)

    elif len(file_paths) > 1:
        for file in file_paths:
            df = pd.read_csv(file, dtype=str)
            try:
                df[join_on] = df[join_on].apply(lambda x: Path(x).stem).str.replace(".png", "",
                                                                                    regex=False).str.replace(
                    ".dcm", "", regex=False)
            except KeyError:
                if join_on == 'name':
                    df = try_to_make_name_column(df)
                    if df is None:
                        raise KeyError(f"Column 'name' not found in {file}.")
                else:
                    raise KeyError(f"Column '{join_on}' not found in {file}.")

            dfs.append(tidy_df(df))

        df_merged = None
        lengths = []
        for i, df in enumerate(dfs):
            if i == 0:
                df_merged = df
            else:
                df_merged = df_merged.merge(df, on=join_on, how=how)
            lengths.append(len(df_merged))
            if i > 0:
                if lengths[i] == lengths[i-1]:
                    logging.warning(f"WARNING: Failed to merge any data for {file_paths[i]}.")

    else:
        raise ValueError("No data files provided.")

    return df_merged


def fill_missing_categories(data: pd.DataFrame) -> pd.DataFrame:
    """
    Fills NaN values in categorical columns with 'Missing'.

    :param data: Original DataFrame.
    :return: DataFrame with filled values.
    """
    for col in data.columns:
        if isinstance(data[col].dtype, pd.CategoricalDtype):
            if 'Missing' not in data[col].cat.categories:
                data[col] = data[col].cat.add_categories('Missing')
            data.loc[data[col].isna(), col] = "Missing"
    return data


def load_value_maps(path: Union[str, Path]) -> dict:
    """
    Loads value maps from a YAML file.

    :param path: The path to the YAML file.
    :return: The value maps.
    """
    with open(path, 'r') as file:
        vms = yaml.safe_load(file)

    if "DONT_PLOT_COLS" not in vms.keys():
        vms["DONT_PLOT_COLS"] = []
    if "RESULTS_MAP" not in vms.keys():
        vms["RESULTS_MAP"] = {
            'positive': 'Positive',
            'negative': 'Negative',
            'invalid': 'Invalid',
            'inconclusive': 'Invalid',
            'error': 'Invalid',
            'unknown': 'Invalid',
            'indeterminate': 'Invalid',
            'incomplete': 'Invalid',

        }
    if "DISEASE_STATUS_MAP" not in vms.keys():
        vms["DISEASE_STATUS_MAP"] = {
            'positive': 'Positive',
            'negative': 'Negative',
            'unknown': 'Unknown',
            'inconclusive': 'Unknown',
            'indeterminate': 'Unknown',
            'incomplete': 'Unknown',
        }
    if "SEX_CATEGORIES" not in vms.keys():
        vms["SEX_CATEGORIES"] = ["M", "F", "Missing"]

    return vms


def load_data_schema(file_path: str, all_dicom_headers: bool = False, focused_columns: bool = False) -> dict:
    """
    Reads a YAML schema file and returns a dictionary.

    :param file_path: Path to the YAML file.
    :param all_dicom_headers: Whether to include all DICOM headers as specified by the schema, default False.
    :param focused_columns: Whether to include only the focused columns as specified by the schema, default False.
    :return: Dictionary with the schema.
    """
    with open(file_path, 'r') as file:
        full_schema = yaml.safe_load(file)

    if focused_columns:
        return full_schema.get('focused_columns', {})

    autoqc_columns = full_schema.get('autoqc_columns', {})
    demogs_columns = full_schema.get('demogs_columns', {})
    dcm_columns = full_schema.get('dcm_columns', {})
    if all_dicom_headers:
        add_dcm_columns = full_schema.get('additional_dcm_columns', {})
        return {**autoqc_columns, **demogs_columns, **dcm_columns, **add_dcm_columns}
    else:
        return {**autoqc_columns, **demogs_columns, **dcm_columns}


def _handle_age_range(x: Any) -> Any:
    """
    Converts age ranges to the mid-point of the range by identifying a hyphen in the string.

    :param x: The age value.
    :return: The mid-point of the age range.
    """
    if isinstance(x, str):
        if "-" in x:
            x_mid = (int(x.split("-")[0]) + int(x.split("-")[1])) / 2
            return x_mid
    return x


def _clean_age(data: pd.DataFrame, column: str) -> pd.DataFrame:
    """
    Cleans the age column by converting to numeric and removing the 'Y' suffix.

    :param data:
    :param column:
    :return:
    """
    data[column] = data[column].apply(_handle_age_range)
    data[column] = data[column].str.strip("Y").str.strip("y")
    data[column] = pd.to_numeric(
        data[column], errors="coerce"
    ).apply(np.floor)
    return data


def _clean_sex(data: pd.DataFrame, column: str, value_map: List[str]) -> pd.DataFrame:
    """
    Cleans the sex column by converting to categorical and mapping the values.

    :param data: The input DataFrame.
    :param column: The column to be cleaned.
    :param value_map: The list of values to map to.
    :return: The cleaned DataFrame.
    """
    try:
        data[column] = data[column].str.upper()
        for cat in data[column].dropna().unique().tolist():
            if cat not in value_map:
                value_map.append(cat)
        data[column] = pd.Categorical(data[column].fillna("Missing"), categories=value_map, ordered=True)
    except AttributeError:
        pass
    return data


def _clean_yes_no(data: pd.DataFrame, column: str) -> pd.DataFrame:
    """
    Cleans the yes/no column by converting to categorical and mapping the values.

    :param data: The input DataFrame.
    :param column: The column to be cleaned.
    :return: The cleaned DataFrame.
    """
    data[column] = data[column].str.upper().map(
        {"Y": "Y", "N": "N", "YES": "Y", "NO": "N"}
    )
    data[column] = pd.Categorical(data[column].fillna("Missing"), categories=["Y", "N", "Missing"], ordered=True)
    return data


def _extract_first_element(value: str) -> Union[str, List[str] or None]:
    """
    Extracts the first element of a string representation of a list,
    or returns the value as is if it's not a list.

    :param value: The string to be processed.
    :return: The first element of the list or the original value.
    """
    try:
        # Attempt to parse the string as a list
        value_as_list = ast.literal_eval(value)
        if isinstance(value_as_list, list) and len(value_as_list) > 0:
            # Return the first element of the list
            return value_as_list[0]
    except:
        # If it's not a list or parsing fails, return the original value
        pass
    return value


def limit_categories(series: pd.Series, limit: int):
    """
    Limits the number of categories in a categorical series to a specified number.

    :param series: The input series.
    :param limit: The maximum number of categories to keep.
    :return: The series with limited categories.
    """

    if len(series.unique()) > limit:
        top_5_categories = series.value_counts().nlargest(limit).index.tolist()
        if 'Missing' not in top_5_categories:
            top_5_categories = series.value_counts().nlargest(limit - 1).index.tolist().append('Missing')

        series = series.apply(lambda x: x if x in top_5_categories else 'Other')
        series = pd.Categorical(series)
        return series

    return series


def _clean_test_results(data: pd.DataFrame, column: str, value_map: Dict[str, str]) -> pd.DataFrame:
    """
    Cleans the test results column by converting to categorical and mapping the values.

    :param data: The input DataFrame.
    :param column: The column to be cleaned.
    :param value_map: The dictionary of values for mapping.
    :return: The cleaned DataFrame.
    """
    data[column] = data[column].str.lower().map(value_map)
    data[column] = pd.Categorical(data[column].fillna("Missing"),
                                  categories=['Positive', 'Negative', 'Unknown', 'Missing'], ordered=True)
    return data


def _clean_disease_status(data: pd.DataFrame, column: str, value_map: Dict[str, str]) -> pd.DataFrame:
    """
    Cleans the disease status column by converting to categorical and mapping the values.

    :param data: The input DataFrame.
    :param column: The column to be cleaned.
    :param value_map: The dictionary of values for mapping.
    :return: The cleaned DataFrame.
    """
    data[column] = data[column].str.lower().map(value_map)
    data[column] = pd.Categorical(data[column].fillna("Missing"),
                                  categories=['Positive', 'Negative', 'Invalid', 'Missing'], ordered=True)
    return data


def clean_dtypes(data: pd.DataFrame, schema_path, value_maps, all_dicom_headers: bool = False) -> pd.DataFrame:
    """
    Cleans the data types of a DataFrame according to a schema.

    :param data: The input DataFrame.
    :param schema_path: The path to the schema file.
    :param value_maps: The value maps.
    :param all_dicom_headers: The flag to include all DICOM headers included in the schema, default False. This will
    make performance slower and may cause issues with large datasets.
    :return: The cleaned DataFrame.
    """
    schema = load_data_schema(schema_path, all_dicom_headers=all_dicom_headers)

    columns_to_use = [col for col in data.columns if col in schema.keys()]
    data = data[columns_to_use]

    for column, dtype in schema.items():
        if column in data.columns:
            if dtype == "sex":
                data = _clean_sex(data, column, value_map=value_maps['SEX_CATEGORIES'])
            elif dtype == "age":
                data = _clean_age(data, column)
            elif column in ["WindowWidth", "WindowCenter"]:
                data[column] = data[column].apply(_extract_first_element)
                data[column] = pd.to_numeric(data[column], errors="coerce")
            elif dtype == "yes_no":
                data = _clean_yes_no(data, column)
            elif dtype == "disease_status":
                data = _clean_disease_status(data, column, value_map=value_maps['DISEASE_STATUS_MAP'])
            elif dtype == "test_result":
                data = _clean_test_results(data, column, value_map=value_maps['RESULTS_MAP'])
            elif dtype == "category":
                data[column] = pd.Categorical(data[column])
            elif dtype == "datetime":
                data[column] = pd.to_datetime(data[column], errors="coerce")
            elif dtype == "dicom_date":
                data[column] = pd.to_datetime(data[column], format="%Y%m%d.%f", errors="coerce")
            elif dtype == "dicom_time":
                data[column] = pd.to_datetime(data[column], format="%H%M%S.%f", errors="coerce")
            elif dtype == "float":
                data[column] = pd.to_numeric(data[column], errors="coerce")
            elif dtype == "int":
                data[column] = pd.to_numeric(data[column], errors="coerce").astype("Int64")
            elif dtype == "str" or dtype == "string" or dtype == "path" or dtype == "file" or dtype == "url":
                data[column] = data[column].astype(str)
            elif dtype == "object":
                data[column] = data[column].astype('object')
            elif dtype == "code_sequence":
                data[column] = data[column].astype(str)
                data[f"{column}-CodeMeaning"] = data[column].str.extract(
                    r"\(0008, 0104\) Code Meaning\s+[A-Z]+:\s*'([^']*)'"
                )
            else:
                data[column] = data[column].astype(str)

    # Fill nan with 'Missing' for categorical columns
    data = fill_missing_categories(data)

    data = data.dropna(axis=1, how='all')
    data = data.drop_duplicates(ignore_index=True)

    return data


# def clean_stored_dtypes(data: pd.DataFrame, schema_path):
#     schema = load_data_schema(schema_path)
#
#     for column, dtype in schema.items():
#         if column in data.columns:
#             if dtype in ["category", "sex" "yes_no", "disease_status", "test_result"]:
#                 data = pd.Categorical(data[column])
#             elif column in ["WindowWidth", "WindowCenter"]:
#                 data[column] = pd.to_numeric(data[column], errors="coerce")
#             elif dtype in ["datetime", "dicom_date", "dicom_time"]:
#                 data[column] = pd.to_datetime(data[column], errors="coerce")
#             elif dtype in ["float", "age", "numeric"]:
#                 data[column] = pd.to_numeric(data[column], errors="coerce")
#             elif dtype == "int":
#                 data[column] = pd.to_numeric(data[column], errors="coerce").astype("Int64")
#             elif dtype == "object":
#                 data[column] = data[column].astype('object')
#             else:
#                 data[column] = data[column].astype(str)
#
#     return data


def callback_if_pathname_matches(target_path: str):
    """
    Decorator for callbacks that should only run if the pathname matches a specified path.

    :param target_path: The target pathname.
    :return: The decorated function.
    """
    def decorator(func):
        def wrapper(*args, **kwargs):
            if callback_context.inputs['url.pathname'] == target_path:
                return func(*args, **kwargs)
            else:
                raise PreventUpdate

        return wrapper

    return decorator


def create_report_settings_dropdown(cb_id_prefix: str) -> dbc.DropdownMenu:
    """
    Creates a settings dropdown menu for the report pages.

    :param cb_id_prefix: The prefix for the callback IDs.
    :return: The settings dropdown menu.
    """
    def _create_settings_tickbox(cb_id: str, label:str, default_value: Optional[bool] = False) -> dbc.Stack:
        """
        Creates a tickbox for the settings dropdown menu.

        :param cb_id: The callback ID.
        :param label: The label for the tickbox.
        :param default_value: The default value for the tickbox.
        :return: The tickbox.
        """
        return dbc.Stack(
            [
                html.Div(label),
                html.Div(
                    "", className="ms-auto"
                ),
                dbc.Switch(
                    id=cb_id,
                    value=default_value,
                )
            ], direction="horizontal", gap=2,
        )

    def _create_settings_numeric(
            cb_id: str, label: str, default_value: Union[int, None], disabled: Optional[bool] = True
    ) -> dbc.Stack:
        """
        Creates a numeric input for the settings dropdown menu.

        :param cb_id: The callback ID.
        :param label: The label for the numeric input.
        :param default_value: The default value for the numeric input.
        :param disabled: The flag to disable the numeric input.
        :return: The numeric input object.
        """
        return dbc.Stack(
            [
                html.Div(label),
                html.Div(
                    "", className="ms-auto"
                ),
                dbc.Input(id=cb_id, type="number", min=0, step=1, disabled=disabled, value=default_value),
            ], direction="horizontal", gap=2,
        )

    settings_dropdown = dbc.DropdownMenu(
        label="Settings",
        id=f"{cb_id_prefix}_settings_dropdown",
        size="lg",
        # menu_variant="dark",
        children=[
            dbc.DropdownMenuItem(
                _create_settings_tickbox(f"{cb_id_prefix}_minimal_report", "Minimal Report:", default_value=False),
                toggle=False
            ),
            dbc.DropdownMenuItem(
                _create_settings_tickbox(f"{cb_id_prefix}_explorative", "Explorative:", default_value=False),
                toggle=False
            ),
            dbc.DropdownMenuItem(
                _create_settings_tickbox(f"{cb_id_prefix}_focused_report", "Focused Report:", default_value=False),
                toggle=False
            ),
            dbc.DropdownMenuItem(
                _create_settings_tickbox(f"{cb_id_prefix}_continuous_var_interactions", "Continuous Interactions:",
                                         default_value=False), toggle=False
            ),
            dbc.DropdownMenuItem(
                _create_settings_tickbox(f"{cb_id_prefix}_sample_report", "Sample Report:", default_value=False),
                toggle=False
            ),
            dbc.DropdownMenuItem(
                _create_settings_numeric(f"{cb_id_prefix}_sample_size", "Sample Size:", default_value=None,
                                         disabled=True), toggle=False
            ),
            dbc.DropdownMenuItem(
                _create_settings_numeric(f"{cb_id_prefix}_seed", "Sampling Seed:", default_value=42, disabled=True),
                toggle=False
            ),
        ],
    )
    return settings_dropdown


def filter_dataframe_columns(df: pd.DataFrame, dont_plot_cols: List[str]) -> pd.DataFrame:
    """
    Filters a DataFrame to keep only columns that are categorical, numeric, or datetime/date.

    :param df: The input DataFrame.
    :param dont_plot_cols: The list of columns to exclude from the filtered DataFrame.
    :return: A new DataFrame containing only the desired columns.
    """
    _df = df.copy()

    # Filter for numeric, datetime, and categorical columns
    filtered_columns = [
        col for col in _df.columns if is_numeric_dtype(
            _df[col]
        ) or is_datetime64_any_dtype(
            _df[col]
        ) or is_categorical_dtype(
            _df[col]
        ) or col == "name"
    ]

    _df = _df[filtered_columns]

    for column in _df.columns:
        if "prob" in column or column in dont_plot_cols:
            _df = _df.drop(column, axis=1)

    return _df


def get_title_text(x: str) -> str:
    """
    Converts a string to title case and replaces underscores with spaces.

    :param x: The input string.
    :return: The title case string.
    """
    if x.islower():
        return x.title().replace('_', ' ')
    else:
        return x


def get_ydata_schema(
        data: pd.DataFrame,
        schema_path: str,
        all_dicom_headers: Optional[bool] = False,
        focused_report: Optional[bool] = False
) -> Dict[str, str]:
    """
    Gets the schema for the y-data profiling, mapping the values to those recognised by y-data.

    :param data: The input DataFrame.
    :param schema_path: The path to the schema file.
    :param all_dicom_headers: The flag to include all DICOM headers included in the schema, default False.
    :param focused_report: The flag to include only the focused columns as specified by the schema, default False.
    :return: The schema for the y-data profiling.
    """
    profile_schema = load_data_schema(
        schema_path, all_dicom_headers=all_dicom_headers, focused_columns=focused_report
    )
    schema_type_map = {
        "yes_no": "boolean",
        "category": "categorical",
        "sex": "categorical",
        "age": "numeric",
        "datetime": "datetime",
        "dicom_date": "date",
        "dicom_time": "datetime",
        "float": "numeric",
        "int": "numeric",
        "disease_status": "categorical",
        "test_result": "categorical",
        "str": "categorical",
    }
    profile_schema = {k: schema_type_map.get(v, v) for k, v in profile_schema.items()}
    profile_schema = {key: value for key, value in profile_schema.items() if key in data.columns}
    return profile_schema


def create_plot_button_group(
        cb_id: str,
        label: str,
        options: list,
        value: Union[bool, str, None]
) -> dbc.Container:
    """
    Creates a button group for selecting a plot type.

    :param cb_id: The callback ID.
    :param label: The label for the button group.
    :param options: The list of options for the button group.
    :param value: The default value for the button group.
    :return: The button group.
    """
    return dbc.Container([
        dbc.Row(
            [
                dbc.Col([
                    dbc.Label(label),
                ],
                    width="auto", className="g-0 me-2 pt-1"),
                dbc.Col([
                    dbc.RadioItems(
                        id=cb_id,
                        className="btn-group",
                        inputClassName="btn-check",
                        labelClassName="btn btn-outline-primary",
                        labelCheckedClassName="active",
                        options=options,
                        value=value,
                    )
                ], className="g-0 radio-group d-flex justify-contents-end", width="auto",
                )
            ], className="d-flex justify-contents-end", justify="end", align='center',
        ),
    ])

