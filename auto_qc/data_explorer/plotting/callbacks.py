"""
callbacks.py: Callbacks for the interactive plotting page of the data explorer.

This file contains the callbacks for the interactive plotting page of the data explorer.
"""

import dash
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
from dash import dcc
import pandas as pd

from .plotting import (generate_plotting_table, generate_dist_plot, generate_pie_plot, generate_box_plot,
                      generate_violin_plot, join_plot_button_groups, dist_plot_bar_mode_button_group,
                      dist_plot_add_plot_button_group, box_plot_points_button_group, violin_plot_mode_button_group,
                      violin_plot_box_button_group, violin_plot_points_button_group)
from ..utils import callback_if_pathname_matches


@dash.callback(
    Output("plotting-plot-card-content", "children"),
    [Input("plotting-plot-card-tabs", "active_tab"),
     Input('url', 'pathname')])
@callback_if_pathname_matches('/plotting')
def tab_content(active_tab: str, url: str):
    """
    Returns the content of the plotting tab.

    :param active_tab: The active tab
    :param url: The current url
    :return: The content of the plotting tab
    """
    if active_tab == "plotting_dist-tab":
        return dbc.Container([
            join_plot_button_groups([dist_plot_bar_mode_button_group, dist_plot_add_plot_button_group]),
            dbc.Row([dcc.Graph(id='plotting_dist-plot')], className='g-0', style={'height': '90%'}),
        ], className='h-100 g-0')
    elif active_tab == "plotting_pie-tab":
        return dbc.Container([
            dbc.Row([dcc.Graph(id='plotting_pie-plot')], className='g-0', style={'height': '90%'})
        ], className='h-100 g-0')
    elif active_tab == "plotting_box-tab":
        return dbc.Container([
            box_plot_points_button_group,
            dbc.Row([dcc.Graph(id='plotting_box-plot')], className='g-0', style={'height': '90%'})
        ], className='h-100 g-0')
    elif active_tab == "plotting_violin-tab":
        return dbc.Container([
            join_plot_button_groups(
                [violin_plot_mode_button_group, violin_plot_box_button_group, violin_plot_points_button_group]),
            dbc.Row([dcc.Graph(id='plotting_violin-plot')], className='g-0', style={'height': '90%'})
        ], className='h-100 g-0')


@dash.callback(
    Output('plotting-table', 'children'),
    [Input('x-variable-dropdown', 'value'),
     Input('y-variable-dropdown', 'value'),
     Input('by-variable-dropdown', 'value'),
     Input('url', 'pathname')],
    [State("dataframe", 'data'),
     State("dtypes", 'data')]
)
@callback_if_pathname_matches('/plotting')
def update_plotting_table(
        x_value: str,
        y_value: str,
        by_value: str,
        url: str,
        json_data: str,
        dtypes: dict
):
    """
    Returns the content of the plotting table.

    :param x_value: The x value
    :param y_value: The y value
    :param by_value: The by value
    :param url: The current url
    :param json_data: The dataframe in a json format
    :param dtypes: The dtypes of the dataframe
    :return: The content of the plotting table
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)
    return generate_plotting_table(df, x_value, y_value, by_value)


@dash.callback(
    Output('plotting_dist-plot', 'figure'),
    [Input('x-variable-dropdown', 'value'),
     Input('by-variable-dropdown', 'value'),
     Input('dist_plot_add_plot-radios', 'value'),
     Input('dist_plot_bar-radios', 'value'),
     Input('url', 'pathname')],
    [State("dataframe", 'data'),
     State("dtypes", 'data'),
     State('cat-limit', 'data')]
)
@callback_if_pathname_matches('/plotting')
def update_dist_plot(
        x_value: str,
        by_value: str,
        dist_plot_radios: str,
        dist_plot_bar_mode: str,
        url: str,
        json_data: str,
        dtypes: dict,
        cat_limit: int
) -> dbc.Container:
    """
    Updates the content of the distribution plot according to the selected values.

    :param x_value: The selected x value
    :param by_value: The selected by value
    :param dist_plot_radios: The selected distribution plot radio button
    :param dist_plot_bar_mode: The selected distribution plot bar mode
    :param url: The current url
    :param json_data: The dataframe in a json format
    :param dtypes: The dtypes of the dataframe
    :param cat_limit: The category limit for the categorical variables
    :return: The content of the distribution plot
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)
    return generate_dist_plot(df, x_value, by_value, dist_plot_radios, dist_plot_bar_mode, cat_limit)


@dash.callback(
    Output('plotting_pie-plot', 'figure'),
    [Input('x-variable-dropdown', 'value'),
     Input('by-variable-dropdown', 'value'),
     Input('url', 'pathname')],
    [State("dataframe", 'data'),
     State("dtypes", 'data'),
     State('cat-limit', 'data')]
)
@callback_if_pathname_matches('/plotting')
def update_pie_plot(
        x_value: str,
        by_value: str,
        url: str,
        json_data: str,
        dtypes: dict,
        cat_limit: int
) -> dbc.Container:
    """
    Updates the content of the pie plot according to the selected values.

    :param x_value: The selected x value
    :param by_value: The selected by value
    :param url: The current url
    :param json_data: The dataframe in a json format
    :param dtypes: The dtypes of the dataframe
    :param cat_limit: The category limit for the categorical variables
    :return: The content of the pie plot
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)
    return generate_pie_plot(df, x_value, by_value, cat_limit)


@dash.callback(
    Output('plotting_box-plot', 'figure'),
    [Input('x-variable-dropdown', 'value'),
     Input('y-variable-dropdown', 'value'),
     Input('by-variable-dropdown', 'value'),
     Input('box_plot_points-radios', 'value'),
     Input('url', 'pathname')],
    [State("dataframe", 'data'),
     State("dtypes", 'data'),
     State('cat-limit', 'data')]
)
@callback_if_pathname_matches('/plotting')
def update_box_plot(
        x_value: str,
        y_value: str,
        by_value: str,
        box_plot_points: bool,
        url: str,
        json_data: str,
        dtypes: dict,
        cat_limit: int
) -> dbc.Container:
    """
    Updates the content of the box plot according to the selected values.

    :param x_value: The selected x value
    :param y_value: The selected y value
    :param by_value: The selected by value
    :param box_plot_points: Whether to show the box plots points
    :param url: The current url
    :param json_data: The dataframe in a json format
    :param dtypes: The dtypes of the dataframe
    :param cat_limit: The category limit for the categorical variables
    :return: The content of the box plot
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)
    return generate_box_plot(df, x_value, y_value, by_value, box_plot_points, cat_limit)


@dash.callback(
    Output('plotting_violin-plot', 'figure'),
    [Input('x-variable-dropdown', 'value'), Input('y-variable-dropdown', 'value'),
     Input('by-variable-dropdown', 'value'),
     Input('violin_plot_mode-radios', 'value'), Input('violin_plot_box-radios', 'value'),
     Input('violin_plot_points-radios', 'value'),
     Input('url', 'pathname')],
    [State("dataframe", 'data'),
     State("dtypes", 'data'),
     State('cat-limit', 'data')]
)
@callback_if_pathname_matches('/plotting')
def update_violin_plot(
        x_value: str,
        y_value: str,
        by_value: str,
        violin_plot_mode: str,
        violin_plot_box: bool,
        violin_plot_points: str,
        url: str,
        json_data: str,
        dtypes: dict,
        cat_limit: int
) -> dbc.Container:
    """
    Updates the content of the violin plot according to the selected values.

    :param x_value: The selected x value
    :param y_value: The selected y value
    :param by_value: The selected by value
    :param violin_plot_mode: The selected violin plot mode
    :param violin_plot_box: Whether to show the box plot has been selected or not
    :param violin_plot_points: The selected violin plot points option
    :param url: The current url
    :param json_data: The dataframe in a json format
    :param dtypes: The dtypes of the dataframe
    :param cat_limit: The category limit for the categorical variables
    :return: The content of the violin plot
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)
    return generate_violin_plot(
        df, x_value, y_value, by_value, violin_plot_mode, violin_plot_box, violin_plot_points, cat_limit
    )


@dash.callback(
    Output('y-variable-dropdown', 'disabled'),
    [Input('plotting-plot-card-tabs', 'active_tab'),
     Input('url', 'pathname')]
)
@callback_if_pathname_matches('/plotting')
def deactivate_y_dropdown(active_tab: str, url: str) -> bool:
    """
    Deactivates the y variable dropdown if the active tab is not the distribution plot.

    :param active_tab: The active tab
    :param url: The current url
    :return: True if the y variable dropdown should be disabled, False otherwise
    """
    if active_tab in ["plotting_dist-tab", "plotting_pie-tab"]:
        return True
    return False
