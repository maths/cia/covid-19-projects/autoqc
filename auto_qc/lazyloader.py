"""
This module provides classes for lazy loading of modules and attributes in Python. This can help to
reduce initial startup latency in some applications, by ensuring that the import and initialization
of a module or the computation of an attribute only happens when it is first used.

The `LazyLoader` class lazily loads a module, specifically designed with TensorFlow in mind but
can be used for any other module as well. The module is only imported and loaded into memory
when an attribute of the module is accessed. This helps to reduce startup latency in applications
that only use certain functionality from TensorFlow or any other hefty module.

The `LazyAttrib` class lazily computes an attribute of an object. The attribute is only computed
when it is accessed, and then it's stored for future use. This can save computation time if the
attribute is computationally intensive and only occasionally used.

Examples
--------
Here is how you might use the `LazyLoader` to lazily import TensorFlow:

    tf = LazyLoader("tensorflow")
    # TensorFlow is not actually imported yet
    print(tf.__version__)
    # Now TensorFlow gets imported and __version__ is printed

Here is how you might use the `LazyAttrib` to lazily compute an attribute:

    class MyClass:
        def __init__(self):
            self._expensive_attr = LazyAttrib(self, '_expensive_compute')

        def _expensive_compute(self):
            # some expensive computation here
            result = ...
            return result

        @property
        def expensive_attr(self):
            return self._expensive_attr.attrib

    my_instance = MyClass()
    print(my_instance.expensive_attr)
    # Now the expensive computation gets done and the result is printed
"""


import importlib
import types
from typing import Optional

class LazyLoader(types.ModuleType):
    """
    This class is used to lazily load a module. The module isn't actually loaded
    until one of its functions or attributes is called.

    :param module_name: The name of the module to be lazily loaded
    :param submod_name: The name of the submodule to be lazily loaded
    :ivar _module_name: The name of the module to be loaded
    :vartype _module_name: str
    :ivar _mod: The module object, once it's loaded
    :vartype _mod: Optional[ModuleType]
    """
    def __init__(self, module_name: str = "tensorflow", submod_name: Optional[str] = None) -> None:
        self._module_name = "{}{}".format(
            module_name, submod_name and ".{}".format(submod_name) or ""
        )
        self._mod = None
        super().__init__(self._module_name)

    def _load(self) -> types.ModuleType:
        """
        Load the module if it has not been loaded yet.

        :returns: The module object
        """
        if self._mod is None:
            self._mod = importlib.import_module(self._module_name)
        return self._mod

    def __getattr__(self, attrb: str):
        """
        Load the module and return the attribute.

        :param attrb: The attribute to be returned
        :returns: The attribute of the loaded module
        """
        return getattr(self._load(), attrb)

    def __dir__(self) -> list:
        """
        Return a list of the attributes of the module.

        :returns: List of attributes of the loaded module
        """
        return dir(self._load())


class LazyAttrib:
    """
    This class is used to lazily load an attribute of an object.

    :param obj: The object that has the attribute
    :param attr_name: The name of the attribute to be lazily loaded
    :ivar _obj: The object that has the attribute
    :vartype _obj: any
    :ivar _attr_name: The name of the attribute to be loaded
    :vartype _attr_name: str
    """
    def __init__(self, obj, attr_name: str) -> None:
        self._obj = obj
        self._attr_name = attr_name

    def __getattr__(self, name: str):
        """
        Return the attribute of the loaded object.

        :param name: The name of the attribute
        :returns: The attribute of the loaded object
        """
        return getattr(self.attrib, name)

    def __call__(self, *args, **kwargs):
        """
        Call the attribute if it's callable.

        :returns: The return value of the attribute, if it's callable
        """
        return self.attrib(*args, **kwargs)

    @property
    def attrib(self):
        """
        Get the attribute of the object.

        :returns: The attribute of the object
        """
        return getattr(self._obj, self._attr_name)

