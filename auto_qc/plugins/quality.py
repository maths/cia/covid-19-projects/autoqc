from auto_qc.classification.quality import contrast_quality_ratio
from auto_qc.utils import error_catcher
from tqdm import tqdm
from yapsy.IPlugin import IPlugin


class Quality(IPlugin):
    @error_catcher
    def run(self, message, details):
        classname = self.__class__.__name__

        res = {}
        with tqdm(total=100) as pbar:
            pbar.set_description(f"{classname} - calculating image quality")
            pbar.update(10)
            array = message["array"]
            q = contrast_quality_ratio(array)
            pbar.update(100)

        res.update({"stop": False, "image_quality": float(q)})
        res.update({"array": message["array"], "hdr": message["hdr"]})
        return res
