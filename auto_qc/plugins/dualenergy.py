import time

from auto_qc.utils import error_catcher
from tqdm import tqdm
from yapsy.IPlugin import IPlugin


class DualEnergy(IPlugin):
    @error_catcher
    def run(self, message, details):
        classname = self.__class__.__name__

        with tqdm(total=100) as pbar:
            # Step 1: Read headers
            pbar.set_description(f"{classname} - read headers")
            hdr = message["hdr"]
            pbar.update(50)
            # Step 1: Read headers
            pbar.set_description(f"{classname} - check headers")
            res = None
            if "ExposureControlModeDescription" in hdr:
                try:
                    if hdr["ImageType"] != "ORIGINAL":
                        res = {"status": "Possible_Dual_Energy", "stop": True}
                except KeyError:
                    res = {
                        "status": "No_Image_Type_in_Poss_Dual_Energy_CXR",
                        "stop": True,
                    }

            try:
                if hdr["Modality"] not in ["CR", "DX"]:
                    res = {"status": "Wrong_Modality_" + hdr["Modality"], "stop": True}
            except KeyError:
                res = {"status": "No_Modality_Header", "stop": True}

            pbar.update(100)

        res = res or {"status": "OK", "stop": False}
        res.update({"array": message["array"], "hdr": message["hdr"]})
        return res
